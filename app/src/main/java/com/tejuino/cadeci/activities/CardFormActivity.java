package com.tejuino.cadeci.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.tejuino.cadeci.BuildConfig;
import com.tejuino.cadeci.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.conekta.conektasdk.Card;
import io.conekta.conektasdk.Conekta;
import io.conekta.conektasdk.Token;

import static com.tejuino.cadeci.activities.PaymentMethodsActivity.registerPayment;
import static com.tejuino.cadeci.activities.OrderTicketsListActivity.REQUEST_PAYMENT_DONE;
import static com.tejuino.cadeci.utils.Repository.getFieldText;
import static com.tejuino.cadeci.utils.Repository.isFormCompleted;

public class CardFormActivity extends AppCompatActivity {

    private static final String TAG = "CardForm";
    private Context mContext;
    View v;
    private List<String> conektaTokens = new ArrayList<>();

    public static Intent newIntent(Context packageContext) {
        Intent i = new Intent(packageContext, CardFormActivity.class);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_form);

        mContext = this;
        v = findViewById(android.R.id.content);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(R.string.card_form_bar_title_label);
        toolbar.findViewById(R.id.toolbar_back).setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_PAYMENT_DONE) {
            if(resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                finish();
            }
        }
    }

    public void onClick(View v) {
        View view = getWindow().getDecorView().getRootView();
        switch (v.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
            case R.id.paymentBtn:
                if (isFormCompleted(view, R.id.formLayout)) {
                    conektaPayment();
                }
                break;
        }
    }

    private void conektaPayment() {
        Activity activity = CardFormActivity.this;

        Conekta.setPublicKey(BuildConfig.CONEKTA_KEY);
        Conekta.setApiVersion("1.0.0");                       //optional
        Conekta.collectDevice(activity);

        Card card = new Card(getFieldText(v, R.id.nameField), getFieldText(v, R.id.cardNoField), getFieldText(v, R.id.cvvField), getFieldText(v, R.id.monthField), getFieldText(v, R.id.yearField));

//        for (int i = 0; i < 20; i++) {
            Token token = new Token(activity);
            token.onCreateTokenListener(new Token.CreateToken() {
                @Override
                public void onCreateTokenReady(JSONObject data) {
                    try {
                        String token = (String) data.get("id");
                        Log.d("Conekta id", token);
                        registerPayment(mContext, "conekta", token);
                        conektaTokens.add(token);
                    } catch (Exception err) {
                        //Do something on error
                    }
                }
            });
            token.create(card);
//        }
        Log.d("Conekta id", "finished");
    }
}
