package com.tejuino.cadeci.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;
import com.tejuino.cadeci.models.ApiResponse;
import com.tejuino.cadeci.models.Comment;
import com.tejuino.cadeci.R;
import com.tejuino.cadeci.views.CircleTransform;
import com.tejuino.cadeci.network.ApiConfig;
import com.tejuino.cadeci.network.ApiSingleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.tejuino.cadeci.network.ApiConfig.comments;
import static com.tejuino.cadeci.utils.Repository.hideKeyboard;
import static com.tejuino.cadeci.utils.Repository.getFieldText;

public class CommentsActivity extends AppCompatActivity {

    private static final String TAG = "CommentsActivity";
    private static final String EXTRA_POSTID = "com.cadeci.postId";
    private Context mContext;
    private EditText mInputField;
    private int mPostId;
    private CommRecyclerAdapter mCommentsAdapter;
    private List<Comment> mCommentList = new ArrayList<>();
    private String mParentId = "0";
    private RecyclerView mCommentsRv;
    private static int maxInputChars = 300;
    private ImageButton mSendBtn;
    private RelativeLayout mCommentReplyBox;
    private TextView mCommReplyTitle;
    private View mView;

    public static Intent newIntent(Context packageContext, int postId) {
        Intent i = new Intent(packageContext, CommentsActivity.class);
        i.putExtra(EXTRA_POSTID, postId);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);

        mContext = this;
        mView = findViewById(android.R.id.content);

        // Toolbar config
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText("Comentarios");
        toolbar.findViewById(R.id.toolbar_back).setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);

        mPostId = getIntent().getIntExtra(EXTRA_POSTID, 0);

        // News Comments
        mCommentsRv = (RecyclerView) findViewById(R.id.commentsRV);
        mCommentsRv.setLayoutManager(new LinearLayoutManager(mContext));
        mCommentsAdapter = new CommRecyclerAdapter(mCommentList, mContext, new CommRecyclerAdapter.OnItemClickedListener() {
            @Override
            public void onClick(View v, Comment comment) {
                mParentId = String.valueOf(comment.getParentId());
                showCommReplyBox(comment);
            }
        });
        mCommentsRv.setAdapter(mCommentsAdapter);
        mCommentsRv.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v,
                                       int left, int top, int right, int bottom,
                                       int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (bottom < oldBottom) {
                    mCommentsRv.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mCommentsRv.scrollToPosition(
                                    mCommentsRv.getAdapter().getItemCount() - 1);
                        }
                    }, 50);
                }
            }
        });

        // Send Button
        mSendBtn = (ImageButton) findViewById(R.id.message_send_button);

        // Input Field config
        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    int type = Character.getType(source.charAt(i));
                    //System.out.println("Type : " + type);
                    if (type == Character.SURROGATE || type == Character.OTHER_SYMBOL) {
                        return "";
                    }
                }
                return null;
            }
        };
        mInputField = (EditText) findViewById(R.id.message_input);
        mInputField.setFilters(new InputFilter[]{
                new InputFilter.LengthFilter(maxInputChars),
                filter});
        mInputField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().isEmpty()) { // There's text available
                    mSendBtn.setColorFilter(getResources().getColor(R.color.colorAccent));
                    mSendBtn.setClickable(true);
                } else {
                    mSendBtn.setColorFilter(getResources().getColor(R.color.commentsSendButton));
                    mSendBtn.setClickable(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mCommentReplyBox = (RelativeLayout) findViewById(R.id.commentReplyBox);
        mCommReplyTitle = (TextView) findViewById(R.id.commReplyBoxTitle);

        loadPostComments(false);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.commReplyBoxBtn:
                hideCommReplyBox();
                break;
            case R.id.message_send_button:
                sendComment();
                break;
            case R.id.toolbar_back:
                finish();
                break;
        }
    }

    private void showCommReplyBox(Comment comment) {
        mCommentReplyBox.setVisibility(View.VISIBLE);
        mCommReplyTitle.setText(comment.getUser().getName() + " " + comment.getUser().getLastName());
        mInputField.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(mInputField, InputMethodManager.SHOW_IMPLICIT);
    }

    private void hideCommReplyBox() {
        mParentId = "0";
        mCommentReplyBox.setVisibility(View.GONE);
        mCommReplyTitle.setText("");
    }

    private void loadPostComments(final boolean isReply) {
        // Access the RequestQueue through your singleton class.
        ApiSingleton.getInstance(mContext)
                .addToRequestQueue(new ApiSingleton.GsonRequest(ApiConfig.posts + "/" + mPostId + "/comments",
                        ApiResponse.class,
                        Request.Method.GET,
                        ApiConfig.getUnauthenticatedHeaders(), null, true,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());
                                ApiResponse res = (ApiResponse) response;
                                mCommentList.clear();
                                List<Comment> comments = res.data.comments;

                                // Add replies to the same list of comments
                                List<Comment> completeComments = new ArrayList<>();
                                for (Comment comment : comments) {
                                    comment.setCommType(0);
                                    comment.setParentId(comment.getId());
                                    completeComments.add(comment);
                                    if (!comment.getReplies().isEmpty()) {
                                        for (Comment reply : comment.getReplies()) {
                                            reply.setCommType(1);
                                            reply.setParentId(comment.getId());
                                            completeComments.add(reply);
                                        }
                                    }
                                }
                                mCommentList.addAll(completeComments);
                                mCommentsAdapter.notifyDataSetChanged();
                                if (isReply)
                                    mCommentsRv.scrollToPosition(mCommentList.size() - 1);

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                    }
                }));

    }

    private void sendComment() {
        mSendBtn.setEnabled(false);

        HashMap<String, String> params = new HashMap<>();
        params.put("comment", getFieldText(mInputField));
        params.put("parent_id", mParentId);

        // Access the RequestQueue through your singleton class.
        ApiSingleton.getInstance(mContext)
                .addToRequestQueue(new ApiSingleton.GsonRequest(String.format(comments, mPostId),
                        ApiResponse.class,
                        Request.Method.POST,
                        ApiConfig.getAuthHeaders(), params, true,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());
                                ApiResponse res = (ApiResponse) response;

                                mInputField.setText("");
                                mInputField.clearFocus();
                                mSendBtn.setEnabled(true);
//                                InputMethodManager imm = (InputMethodManager) getSystemService(mContext.INPUT_METHOD_SERVICE);
//                                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                                // Check if no view has focus:

                                hideKeyboard(CommentsActivity.this);

                                loadPostComments(true);
                                hideCommReplyBox();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                        mSendBtn.setEnabled(true);
                    }
                }));

    }
}

class CommRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int TYPE_COMMENT = 0;
    public static final int TYPE_REPLY = 1;

    private final List<Comment> mValues;
    private final Context mContext;
    private final OnItemClickedListener mListener;

    public CommRecyclerAdapter(List<Comment> items, Context context, OnItemClickedListener listener) {
        mValues = items;
        mContext = context;
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case TYPE_COMMENT:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_comment, parent, false);
                return new CommentsNewsHolder(view);
            case TYPE_REPLY:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_comment_reply, parent, false);
                return new ReplyNewsHolder(view);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, int position) {
        Comment item = mValues.get(position);
        switch (getItemViewType(position)) {
            case TYPE_COMMENT:
                final CommentsNewsHolder commentsNewsHolder = (CommentsNewsHolder) viewHolder;
                commentsNewsHolder.setItem(item);
                try {
                    commentsNewsHolder.mUsrNameTxt.setText(item.getUser().getName());
                    Picasso.with(mContext)
                            .load(item.getUser().getImage())
                            .resize(100, 100)
                            .centerCrop()
                            .transform(new CircleTransform())
                            .into(commentsNewsHolder.mImageView);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                commentsNewsHolder.mMessageTxt.setText(item.getMessage());
                commentsNewsHolder.mDateTxt.setText(item.getCreatedAtString());
                break;
            case TYPE_REPLY:
                final ReplyNewsHolder replyNewsHolder = (ReplyNewsHolder) viewHolder;
                replyNewsHolder.setItem(item);
                replyNewsHolder.mUsrNameTxt.setText(item.getUser().getName());
                replyNewsHolder.mMessageTxt.setText(item.getMessage());
                Picasso.with(mContext)
                        .load(item.getUser().getImage())
                        .resize(100, 100)
                        .centerCrop()
                        .transform(new CircleTransform())
                        .into(replyNewsHolder.mImageView);
                replyNewsHolder.mDateTxt.setText(item.getCreatedAtString());
                break;
            default:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class CommentsNewsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final View mView;
        public final TextView mUsrNameTxt;
        public final TextView mSubtitleTxt;
        public final TextView mMessageTxt;
        public final TextView mDateTxt;
        public final ImageView mImageView;
        private Comment mItem;

        public CommentsNewsHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            mView = view;
            mUsrNameTxt = (TextView) view.findViewById(R.id.titleTxt);
            mSubtitleTxt = (TextView) view.findViewById(R.id.mainSubtitleTxt);
            mMessageTxt = (TextView) view.findViewById(R.id.descriptionTxt);
            mDateTxt = (TextView) view.findViewById(R.id.dateTxt);
            mImageView = (ImageView) view.findViewById(R.id.coverImg);
        }

        public void setItem(Comment item) {
            mItem = item;
        }

        @Override
        public void onClick(View v) {
            mListener.onClick(v, mItem);
        }
    }

    public class ReplyNewsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final View mView;
        public final TextView mUsrNameTxt;
        public final TextView mMessageTxt;
        public final TextView mDateTxt;
        public final ImageView mImageView;
        private Comment mItem;

        public ReplyNewsHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            mView = view;
            mUsrNameTxt = (TextView) view.findViewById(R.id.titleTxt);
            mMessageTxt = (TextView) view.findViewById(R.id.descriptionTxt);
            mDateTxt = (TextView) view.findViewById(R.id.dateTxt);
            mImageView = (ImageView) view.findViewById(R.id.coverImg);
        }

        public void setItem(Comment item) {
            mItem = item;
        }

        @Override
        public void onClick(View v) {
            mListener.onClick(v, mItem);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mValues.get(position).getCommType();
    }

    public interface OnItemClickedListener {
        void onClick(View v, Comment comment);
    }
}

