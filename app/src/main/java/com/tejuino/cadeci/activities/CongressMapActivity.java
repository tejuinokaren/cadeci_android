package com.tejuino.cadeci.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;
import com.tejuino.cadeci.models.ApiResponse;
import com.tejuino.cadeci.network.ApiConfig;
import com.tejuino.cadeci.network.ApiSingleton;
import com.tejuino.cadeci.R;

/**
 * Created by karen on 21/06/17.
 */

public class CongressMapActivity extends AppCompatActivity {

    private static final String EXTRA_MAP_URL = "cadeci.mapUrl";
    private static final String TAG = "cadeci.congressMap";
    private Context mContext;

    public static Intent newIntent(Context packageContext, String mapUrl) {
        Intent i = new Intent(packageContext, CongressMapActivity.class);
        i.putExtra(EXTRA_MAP_URL, mapUrl);
        return i;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        mContext = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(R.string.map_title_bar_label);
        toolbar.findViewById(R.id.toolbar_back).setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);

        Picasso.with(mContext)
                .load(getIntent().getStringExtra(EXTRA_MAP_URL))
                .resize(1000, 1000)
                .into((ImageView) findViewById(R.id.mapImg));
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
        }
    }

    public static void loadCongressMap(final Context ctx) {
        // Access the RequestQueue through your singleton class.
        ApiSingleton.getInstance(ctx)
                .addToRequestQueue(new ApiSingleton.GsonRequest(ApiConfig.lastCongress,
                        ApiResponse.class,
                        Request.Method.GET,
                        ApiConfig.getUnauthenticatedHeaders(), null, true,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());
                                ApiResponse res = (ApiResponse) response;
                                ctx.startActivity(CongressMapActivity.newIntent(ctx, res.data.congress.getImageMap()));
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                    }
                }));
    }
}