package com.tejuino.cadeci.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.tejuino.cadeci.R;

public class FoundationActivity extends AppCompatActivity {

    private Context mContext;

    public static Intent newIntent(Context packageContext) {
        Intent i = new Intent(packageContext, FoundationActivity.class);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foundation);

        mContext = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(R.string.foundation_bar_title_label);
        toolbar.findViewById(R.id.toolbar_back).setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
        }
    }
}
