package com.tejuino.cadeci.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.tejuino.cadeci.models.ApiResponse;
import com.tejuino.cadeci.models.Order;
import com.tejuino.cadeci.network.ApiConfig;
import com.tejuino.cadeci.network.ApiSingleton;
import com.tejuino.cadeci.R;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import static com.tejuino.cadeci.utils.Repository.getFieldText;
import static com.tejuino.cadeci.utils.Repository.isFormCompleted;

public class InvoiceActivity extends AppCompatActivity {

    private static final String EXTRA_ORDER = "com.cadeci.order";
    private static final String TAG = "com.cadeci.invoice";
    private Context mContext;
//    private EditText mEmailField;
    private View mMainView;
    private Order mOrder;

    public static Intent newIntent(Context packageContext, Order order) {
        Intent i = new Intent(packageContext, InvoiceActivity.class);
        i.putExtra(EXTRA_ORDER, (Serializable) order);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice);

        mContext = this;
        mMainView = findViewById(android.R.id.content);
        mOrder = (Order) getIntent().getSerializableExtra(EXTRA_ORDER);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView)toolbar.findViewById(R.id.toolbar_title)).setText(R.string.invoice_bar_title_label);

//        mEmailField = (EditText) findViewById(R.id.emailField);
        ((EditText) findViewById(R.id.rfcField)).setText(mOrder.getInvoiceRfc());
        ((EditText) findViewById(R.id.nameField)).setText(mOrder.getInvoiceName());
        ((EditText) findViewById(R.id.streetField)).setText(mOrder.getInvoiceStreet());
        ((EditText) findViewById(R.id.noextField)).setText(mOrder.getInvoiceExternalNumber());
        ((EditText) findViewById(R.id.nointField)).setText(mOrder.getInvoiceInternalNumber());
        ((EditText) findViewById(R.id.suburbField)).setText(mOrder.getInvoiceSuburb());
        ((EditText) findViewById(R.id.cityField)).setText(mOrder.getInvoiceCity());
        ((EditText) findViewById(R.id.stateField)).setText(mOrder.getInvoiceState());
        ((EditText) findViewById(R.id.zipcodeField)).setText(mOrder.getInvoiceZip());

    }

    public void onClick(View v) {
        View view = getWindow().getDecorView().getRootView();
        switch (v.getId()) {
            case R.id.saveBtn:
                if (isFormCompleted(view, R.id.formLayout)) {
//                    if(isValidEmail(view, mEmailField)) {
                        addInscription();
//                    }
                }
                break;
        }
    }

    private void addInscription() {
        Map<String, String> params = new HashMap<>();
        params.put("invoice_required", "1");
        params.put("invoice_rfc", getFieldText(mMainView, R.id.rfcField));
        params.put("invoice_name", getFieldText(mMainView, R.id.nameField));
        params.put("invoice_street", getFieldText(mMainView, R.id.streetField));
        params.put("invoice_external_number", getFieldText(mMainView, R.id.noextField));
        params.put("invoice_internal_number",getFieldText(mMainView, R.id.nointField));
        params.put("invoice_suburb",getFieldText(mMainView, R.id.suburbField));
        params.put("invoice_city",getFieldText(mMainView, R.id.cityField));
        params.put("invoice_state", getFieldText(mMainView, R.id.stateField));
        params.put("invoice_zip", getFieldText(mMainView, R.id.zipcodeField));

        ApiSingleton.getInstance(mContext)
                .addToRequestQueue(new ApiSingleton.GsonRequest(ApiConfig.invoice,
                        ApiResponse.class,
                        Request.Method.POST,
                        ApiConfig.getAuthHeaders(), params, true,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());
                                ApiResponse res = (ApiResponse) response;
                                Intent i = new Intent(InvoiceActivity.this, OrderTicketsListActivity.class);
                                i.putExtra(OrderTicketsListActivity.EXTRA_ORDER, (Serializable) res.data.order);
                                setResult(RESULT_OK, i);
                                finish();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                    }
                }));

    }
}
