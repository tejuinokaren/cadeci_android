package com.tejuino.cadeci.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.tejuino.cadeci.models.ApiResponse;
import com.tejuino.cadeci.network.ApiConfig;
import com.tejuino.cadeci.network.ApiSingleton;
import com.tejuino.cadeci.R;
import com.tejuino.cadeci.utils.Repository;

import static com.tejuino.cadeci.utils.Repository.getFieldText;
import static com.tejuino.cadeci.utils.SessionManager.sSession;
import static com.tejuino.cadeci.utils.SessionManager.sUser;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "com.cadeci.MainActivity";
    private Context mContext;

    public static Intent newIntent(Context packageContext) {
        Intent i = new Intent(packageContext, LoginActivity.class);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mContext = this;

    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginBtn:
                // Prevent user from being null
                sUser.getInstance().setEmail(getFieldText(findViewById(android.R.id.content), R.id.emailField));
                sUser.getInstance().setPassword(getFieldText(findViewById(android.R.id.content), R.id.passwordField));
                sSession.auth(mContext);
                break;
            case R.id.registerLabel:
                startActivity(RegisterNewUserActivity.newIntent(mContext));
                break;
            case R.id.recoverLabel:
                startActivity(RecoverPasswordActivity.newIntent(mContext));
                break;
            case R.id.privacyLabel:
                loadCongress();
                break;
        }
    }

    private void loadCongress() {
        // Access the RequestQueue through your singleton class.
        ApiSingleton.getInstance(mContext)
                .addToRequestQueue(new ApiSingleton.GsonRequest(ApiConfig.lastCongress,
                        ApiResponse.class,
                        Request.Method.GET,
                        ApiConfig.getUnauthenticatedHeaders(), null, true,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());
                                ApiResponse res = (ApiResponse) response;
                                Repository.openPdfURL(res.data.congress.getPrivacyFile(), mContext);
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                    }
                }));
    }
}
