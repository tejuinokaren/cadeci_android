package com.tejuino.cadeci.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.BottomBarTab;
import com.roughike.bottombar.OnTabSelectListener;
import com.tejuino.cadeci.fragments.NewsFragment;
import com.tejuino.cadeci.fragments.ProfileFragment;
import com.tejuino.cadeci.fragments.ScheduleMainTabFragment;
import com.tejuino.cadeci.fragments.SponsorsFragment;
import com.tejuino.cadeci.fragments.OrdersListFragment;
import com.tejuino.cadeci.R;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "com.cadeci.MainActivity";
    public static final boolean CONGRESS_AVAILABLE = true; // Enables/ Disables congress schedule and reminders visibility depending on whether there's data for the next congress
    private Context mContext;

    private NewsFragment mNewsFragment;
    private ScheduleMainTabFragment mScheduleMainTabFragment;
    private SponsorsFragment mSponsorsFragment;
    private OrdersListFragment mOrdersListFragment;
    private ProfileFragment mProfileFragment;

    private BottomBar mBottomBar;
    private Toolbar mToolbar;
    private boolean mBackPressed = false;

    public static Intent newIntent(Context packageContext) {
        Intent i = new Intent(packageContext, MainActivity.class);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mBackPressed = false;
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (mBackPressed) {
            finish();
        } else {
            mBackPressed = true;
            Toast.makeText(mContext,
                    getResources().getString(R.string.press_again_to_close),
                    Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mBackPressed = false;
                }
            }, 5000);
        }

    }

    private void init() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        mBottomBar = (BottomBar) findViewById(R.id.bottom_navigation);
        BottomBarTab viewItem;
        // Center icons in BottomNav without text
        for (int i = 0; i < mBottomBar.getTabCount(); i++) {
            viewItem = mBottomBar.getTabAtPosition(i);

            //TITLE
            TextView titleTab = (TextView) viewItem.findViewById(com.roughike.bottombar.R.id.bb_bottom_bar_title);
            titleTab.setVisibility(View.GONE);

            // IMAGE
            viewItem.setGravity(Gravity.CENTER_VERTICAL);
        }

        mBottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                mBackPressed = false;
                mToolbar.findViewById(R.id.toolbar_add).setVisibility(View.GONE);
                mToolbar.findViewById(R.id.toolbar_map).setVisibility(View.GONE);

                switch (tabId) {
                    case R.id.navigation_news:
                        if(mNewsFragment == null)
                            mNewsFragment = new NewsFragment().newInstance(1);
                        replaceFragment(mNewsFragment);
                        break;
                    case R.id.navigation_congress:
                        // Check if congress is already available to show schedules or coming soon alert
                        if(CONGRESS_AVAILABLE) {
                            if(mScheduleMainTabFragment == null)
                                mScheduleMainTabFragment = new ScheduleMainTabFragment().newInstance();
                            replaceFragment(mScheduleMainTabFragment);
                        } else {
                            new AlertDialog.Builder(mContext)
                                    .setTitle(R.string.coming_soon_alert_title)
                                    .setMessage(R.string.coming_soon_alert_message)
                                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    }).create().show();
                        }
                        break;
                    case R.id.navigation_tickets:
                        if(mOrdersListFragment == null)
                            mOrdersListFragment = new OrdersListFragment().newInstance();
                        replaceFragment(mOrdersListFragment);
                        break;
                    case R.id.navigation_sponsors:
                        if(mSponsorsFragment == null)
                            mSponsorsFragment = new SponsorsFragment().newInstance();
                        replaceFragment(mSponsorsFragment);
                        break;
                    case R.id.navigation_profile:
                        if(mProfileFragment == null)
                            mProfileFragment = new ProfileFragment().newInstance();
                        replaceFragment(mProfileFragment);
                        break;
                }

            }
        });
    }

    public void replaceFragment (Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the sUser can navigate back
        transaction.replace(R.id.content, fragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();
    }

    public void setToolbarTitle(String title) {
        ((TextView) mToolbar.findViewById(R.id.toolbar_title)).setText(title);
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (makeTranslucent) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    public void performClickBottomNavItem(int itemId) {
        // The "0" in this case is the first tab, and "false" indicates if there should be animation or not.
        mBottomBar.selectTabWithId(itemId);
    }
}
