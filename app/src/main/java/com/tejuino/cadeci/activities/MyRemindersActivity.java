package com.tejuino.cadeci.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.tejuino.cadeci.models.ApiResponse;
import com.tejuino.cadeci.models.Reminder;
import com.tejuino.cadeci.models.Speaker;
import com.tejuino.cadeci.network.ApiConfig;
import com.tejuino.cadeci.network.ApiSingleton;
import com.tejuino.cadeci.R;
import com.tejuino.cadeci.utils.Repository;
import com.tejuino.cadeci.views.SquareLinearLayoutWidth;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by dianakarenms@gmail.com on 04/08/17.
 */

public class MyRemindersActivity extends AppCompatActivity {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private MyRemindersAdapter mRemindersAdapter;
    private List<Reminder> mRemindersList = new ArrayList<>();
    private Repository.PaginationRefreshRecycler mRemindersRV;
    private Context mContext;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static Intent newIntent(Context packageContext) {
        Intent i = new Intent(packageContext, MyRemindersActivity.class);
        return i;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_reminders);

        mContext = this;

        // Toolbar config
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(R.string.my_reminders_bar_title);
        toolbar.findViewById(R.id.toolbar_back).setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);

        mRemindersRV = (Repository.PaginationRefreshRecycler) findViewById(R.id.schedule_recycler);
        mRemindersRV.setLayoutManager(new LinearLayoutManager(mContext));
        mRemindersAdapter = new MyRemindersAdapter(mContext, mRemindersList, new MyRemindersAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(final Reminder reminder, final int position) {
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
                } else {
                    builder = new AlertDialog.Builder(mContext);
                }
                builder.setMessage("¿Deseas eliminar el recordatorio para: \"" + reminder.getTitle() + "\"?")
                        .setPositiveButton("Eliminar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                deleteReminder(reminder.getId(), position);
                                //checkIfEmptyList();
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });
        mRemindersRV.setAdapter(mRemindersAdapter);
        mRemindersRV.setList(mRemindersList)
                .addSwipeRefresh((SwipeRefreshLayout) findViewById(R.id.swiperefresh), new Repository.PaginationRefreshRecycler.SwipeRefreshListener() {
                    @Override
                    public void onRequestData() {
                        loadMyReminders();
                    }
                });

        loadMyReminders();

        ((Button) findViewById(R.id.buyTicketBtn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_OK);
                finish();
            }
        });
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
        }
    }

    private void loadMyReminders() {
        // Access the RequestQueue through your singleton class.
        ApiSingleton.getInstance(mContext)
                .addToRequestQueue(new ApiSingleton.GsonRequest(ApiConfig.myReminders,
                        ApiResponse.class,
                        Request.Method.GET,
                        ApiConfig.getAuthHeaders(), null, true,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());
                                ApiResponse res = (ApiResponse) response;

                                mRemindersList.clear();

                                List<Reminder> reminders = new ArrayList<>();
                                String headerDate = "";
                                for (Reminder reminder : res.data.reminders) {
                                    String reminderDate = reminder.getDate();
                                    if (! reminderDate.equals(headerDate)) {
                                        // Add Header
                                        Reminder header = new Reminder();
                                        header.setType(Reminder.TAG_HEADER);
                                        header.setStart(reminder.getStart());
                                        headerDate = header.getDate();
                                        reminders.add(header);
                                    }

                                    // Add Item
                                    reminders.add(reminder.setType(Reminder.TAG_ITEM));
                                }
                                mRemindersList.addAll(reminders);
                                mRemindersAdapter.notifyDataSetChanged();
                                mRemindersRV.stopRefresh();
                                checkIfEmptyList();

                                mRemindersRV.stopRefresh();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                        mRemindersRV.stopRefresh();
                    }
                }));
    }

    private void checkIfEmptyList() {
        if(mRemindersList.isEmpty()) {
            findViewById(R.id.swiperefresh).setVisibility(View.GONE);
            findViewById(R.id.emptyView).setVisibility(View.VISIBLE);
        }
    }

    private void deleteReminder(int reminderId, final int position) {
        // Access the RequestQueue through your singleton class.
        ApiSingleton.getInstance(mContext)
                .addToRequestQueue(new ApiSingleton.GsonRequest(String.format(ApiConfig.deleteReminder, reminderId),
                        ApiResponse.class,
                        Request.Method.POST,
                        ApiConfig.getAuthHeaders(), null, false,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());
                                ApiResponse res = (ApiResponse) response;
                                Toast.makeText(mContext, R.string.reminder_deleted, Toast.LENGTH_SHORT).show();
                                loadMyReminders();
                                //mRemindersList.remove(position);
                                //mRemindersAdapter.notifyItemRemoved(position);

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
//                        mPaginationRefreshRecycler.stopRefresh();
                    }
                }));
    }
}

class MyRemindersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int TYPE_DUMMY = -1;
    public static final int TYPE_HEADER = 0;
    public static final int TYPE_ITEM = 1;

    private final List<Reminder> mValues;
    private final Context mContext;
    private OnItemClickListener mListener;

    public MyRemindersAdapter(Context context, List<Reminder> items, OnItemClickListener listener) {
        mContext = context;
        mValues = items;
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case TYPE_HEADER:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_my_reminder_header, parent, false);
                return new HeaderSchHolder(view);
            case TYPE_ITEM:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_my_reminder_child, parent, false);
                return new ItemSchHolder(view);
            default:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_my_reminder_child, parent, false);
                return new ItemSchHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, int position) {
        Reminder item = mValues.get(position);
        if(item != null) {
            switch (getItemViewType(position)) {
                case TYPE_HEADER:
                    final HeaderSchHolder headerSchHolder = (HeaderSchHolder) viewHolder;
                    headerSchHolder.setItem(item);
                    headerSchHolder.mWeekDay.setText(item.getScheduleWeekDayString());
                    headerSchHolder.mDay.setText(item.getScheduleDayString());

                    break;
                case TYPE_ITEM:
                    final ItemSchHolder itemSchHolder = (ItemSchHolder) viewHolder;
                    itemSchHolder.setItem(item);
                    itemSchHolder.mTitleTxt.setText(item.getTitle());
                    itemSchHolder.mNameTxt.setText(getSpeakersByComma(item.getSpeakers()));
                    itemSchHolder.mStartTimeTxt.setText(item.getStartHourString());
                    itemSchHolder.mEndTimeTxt.setText(item.getFinishHourString());
                    break;
                default:
                    final ItemSchHolder dummySchHolder = (ItemSchHolder) viewHolder;
                    int backColor = mContext.getResources().getColor(R.color.dummiesBack);
                    dummySchHolder.setItem(item);
                    dummySchHolder.mTitleTxt.setBackgroundColor(backColor);
                    dummySchHolder.mNameTxt.setBackgroundColor(backColor);
                    dummySchHolder.mStartTimeTxt.setBackgroundColor(backColor);
                    dummySchHolder.mEndTimeTxt.setBackgroundColor(backColor);
                    dummySchHolder.mHourCircle.setBackgroundColor(mContext.getResources().getColor(R.color.backgroundPrimaryLight));
                    dummySchHolder.setClickable(false);
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class HeaderSchHolder extends RecyclerView.ViewHolder  {
        public final View mView;
        public final TextView mWeekDay;
        public final TextView mDay;
        private Reminder mItem;

        public HeaderSchHolder(View view) {
            super(view);
            mView = view;
            mWeekDay = (TextView) view.findViewById(R.id.week_day_label);
            mDay = (TextView) view.findViewById(R.id.day_label);
        }

        public void setItem(Reminder item) {
            mItem = item;
        }
    }

    public class ItemSchHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final View mView;
        public final TextView mTitleTxt;
        public final TextView mNameTxt;
        public final TextView mStartTimeTxt;
        public final TextView mEndTimeTxt;
        public final SquareLinearLayoutWidth mHourCircle;
        private Reminder mItem;
        private boolean isClickable = true;

        public ItemSchHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            mView = view;
            mTitleTxt = (TextView) view.findViewById(R.id.title_txt);
            mNameTxt = (TextView) view.findViewById(R.id.name_txt);
            mStartTimeTxt = (TextView) view.findViewById(R.id.start_time_txt);
            mEndTimeTxt = (TextView) view.findViewById(R.id.end_time_txt);
            mHourCircle = (SquareLinearLayoutWidth) view.findViewById(R.id.hourCircle);
        }

        public void setItem(Reminder item) {
            mItem = item;
        }

        public void setClickable(boolean isClickable) {
            this.isClickable = isClickable;
        }

        @Override
        public void onClick(View v) {
            if (isClickable) {
                mListener.onItemClick(mItem, getAdapterPosition());
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        switch (mValues.get(position).getType()) {
            case Reminder.TAG_HEADER:
                return TYPE_HEADER;
            case Reminder.TAG_ITEM:
                return TYPE_ITEM;
            default:
                return -1;
        }
    }

    public interface OnItemClickListener {
        public void onItemClick(Reminder reminder, int position);
    }

    private String getSpeakersByComma(List<Speaker> list) {
        String answer = "";
        Speaker speaker = null;
        for(int i = 0; i < list.size(); i++){
            speaker = list.get(i);
            answer = answer + speaker.getName() + " (" + speaker.getHometown() + ")";
            if(i != list.size()-1) {
                answer = answer + ",\n";
            }
        }
        return answer;
    }
}