package com.tejuino.cadeci.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.tejuino.cadeci.models.ApiResponse;
import com.tejuino.cadeci.models.Comment;
import com.tejuino.cadeci.models.Post;
import com.tejuino.cadeci.models.PostsRelated;
import com.tejuino.cadeci.R;
import com.tejuino.cadeci.views.CircleTransform;
import com.tejuino.cadeci.network.ApiConfig;
import com.tejuino.cadeci.network.ApiSingleton;

import java.util.ArrayList;
import java.util.List;

import static com.tejuino.cadeci.R.id.webView;
import static com.android.volley.VolleyLog.TAG;
import static com.tejuino.cadeci.utils.Repository.animCrossfade;
import static com.tejuino.cadeci.utils.Repository.hideProgressFlower;
import static com.tejuino.cadeci.utils.Repository.showProgressFlower;

/**
 * Created by karen on 03/07/17.
 */

public class NewsDetailActivity extends AppCompatActivity  {

    private static final String EXTRA_NEWS_ITEM = "Cadeci.NewsDetails.NewsItem";
    private Context mContext;
    private static Post sPost;
    private CommentsRecyclerAdapter mCommentsAdapter;
    private int mPostId;
    private Toolbar toolbar;
    private TextView titleTxt, descriptionTxt, dateTxt;
    private ImageView imageView;
    private ImageButton rightBtn;
    private Dialog mDialogTransparent;
    private List<Comment> mCommentsList = new ArrayList<>();

    public static Intent newIntent(Context packageContext, int postId) {
        Intent i = new Intent(packageContext, NewsDetailActivity.class);
        i.putExtra(EXTRA_NEWS_ITEM, postId);
        return i;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);

        mContext = this;

        showProgressFlower(mContext);

        mPostId = getIntent().getIntExtra(EXTRA_NEWS_ITEM, 0);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.findViewById(R.id.toolbar_back).setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);
        rightBtn = (ImageButton) toolbar.findViewById(R.id.toolbar_share);

        // Main Details
        titleTxt = (TextView) findViewById(R.id.titleTxt);
        descriptionTxt = (TextView) findViewById(R.id.descriptionTxt);
        dateTxt = (TextView) findViewById(R.id.dateTxt);
        imageView = (ImageView) findViewById(R.id.coverImg);

        // News Comments
        RecyclerView commentsRv = (RecyclerView) findViewById(R.id.commentsRV);
        commentsRv.setLayoutManager(new LinearLayoutManager(mContext));
        mCommentsAdapter = new CommentsRecyclerAdapter(mCommentsList, mContext, new CommentsRecyclerAdapter.OnCommentsClickListener() {
            @Override
            public void onClick(Integer commentId) {
                startActivity(CommentsActivity.newIntent(mContext, sPost.getId()));
            }
        });
        commentsRv.setAdapter(mCommentsAdapter);

        loadNewsPosts(mPostId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadLastComments();
    }

    @SuppressLint("JavascriptInterface")
    private void init() {
        // Toolbar config
//        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(sPost.getTitle());
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(getResources().getString(R.string.news_bar_title_label));

        rightBtn.setVisibility(View.VISIBLE);
        final Post temp = sPost;
        rightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.addCategory(Intent.CATEGORY_DEFAULT);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, temp.getTitle());
                i.putExtra(Intent.EXTRA_TEXT, temp.getUrl());
                mContext.startActivity(Intent.createChooser(i, "Elige una opción"));
            }
        });
        
        // Related News
        RecyclerView relatedRv = (RecyclerView) findViewById(R.id.relatedRV);
        relatedRv.setLayoutManager(new LinearLayoutManager(mContext));
        relatedRv.setAdapter(new RelatedPostsRecyclerAdapter(mContext, sPost.getPostsRelatedList(), new RelatedPostsRecyclerAdapter.OnRecyclerClickListener() {
            @Override
            public void onClick(int postId) {
//                loadRelatedNewsItem(postId);
                startActivity(NewsDetailActivity.newIntent(mContext, postId));
            }
        }));

        if(sPost.getMessages().isEmpty()) {
            findViewById(R.id.commentsLay).setVisibility(View.GONE);
            ((TextView) findViewById(R.id.watchMoreTxt)).setText(R.string.new_comment_empty_comments_label);
        }

        // Web Content
        final WebView webview = (WebView) findViewById(webView);
        webview.getSettings().setJavaScriptEnabled(true); // enable javascript
        webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webview.loadData(sPost.getContent(), "text/html", "UTF-8");
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                Picasso.with(mContext)
                        .load(sPost.getImage())
                        .centerInside()
                        .resize(500, 500)
                        .into(imageView, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        // Call the "scheduleStartPostponedTransition()" method
                                        // below when you know for certain that the shared element is
                                        // ready for the transition to begin.
//                                        scheduleStartPostponedTransition(imageView);

                                        titleTxt.setBackgroundColor(0);
                                        titleTxt.setText(sPost.getTitle());
                                        descriptionTxt.setBackgroundColor(0);
                                        descriptionTxt.setText(sPost.getExtract());
                                        dateTxt.setBackgroundColor(0);
                                        dateTxt.setText(sPost.getCreatedAtString());

                                        animCrossfade(findViewById(R.id.container));
                                        hideProgressFlower();
                                        findViewById(R.id.bodyLay).setVisibility(View.VISIBLE);
                                    }

                                    @Override
                                    public void onError() {
                                        // ...
                                    }
                                });

            }
        });
    }

    /**
     * Schedules the shared element transition to be started immediately
     * after the shared element has been measured and laid out within the
     * activity's view hierarchy. Some common places where it might make
     * sense to call this method are:
     *
     * (1) Inside a Fragment's onCreateView() method (if the shared element
     *     lives inside a Fragment hosted by the called Activity).
     *
     * (2) Inside a Picasso Callback object (if you need to wait for Picasso to
     *     asynchronously load/scale a bitmap before the transition can begin).
     **/
    private void scheduleStartPostponedTransition(final View sharedElement) {
        sharedElement.getViewTreeObserver().addOnPreDrawListener(
                new ViewTreeObserver.OnPreDrawListener() {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public boolean onPreDraw() {
                        sharedElement.getViewTreeObserver().removeOnPreDrawListener(this);
                        startPostponedEnterTransition();
                        return true;
                    }
                });
    }

    private void loadLastComments() {
        // Access the RequestQueue through your singleton class.
        ApiSingleton.getInstance(mContext)
                .addToRequestQueue(new ApiSingleton.GsonRequest(String.format(ApiConfig.lastComments, mPostId),
                        ApiResponse.class,
                        Request.Method.GET,
                        ApiConfig.getAuthHeaders(), null, false,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());
                                ApiResponse res = (ApiResponse) response;
                                mCommentsList.clear();
                                mCommentsList.addAll(res.data.comments);
                                mCommentsAdapter.notifyDataSetChanged();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                    }
                }));

    }

    private void loadNewsPosts(final int postId) {
        // Access the RequestQueue through your singleton class.
        ApiSingleton.getInstance(mContext)
                .addToRequestQueue(new ApiSingleton.GsonRequest(ApiConfig.posts + "/" + postId,
                        ApiResponse.class,
                        Request.Method.GET,
                        ApiConfig.getUnauthenticatedHeaders(), null, false,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());
                                ApiResponse res = (ApiResponse) response;
                                 sPost = res.data.mPost;
                                init();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                    }
                }));

    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.watchMoreTxt:
                mContext.startActivity(CommentsActivity.newIntent(mContext, sPost.getId()));
                break;
            case R.id.toolbar_back:
//                finish();
                supportFinishAfterTransition();
                break;
        }
    }
}

/**
 * {@link RecyclerView.Adapter} that can display a {@link Post} and makes a call to the
 * specified {@link OnRecyclerClickListener}.
 */
class RelatedPostsRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int TYPE_MINOR = 0;

    private List<PostsRelated> mValues;
    private OnRecyclerClickListener mListener;
    private Context mContext;

    public RelatedPostsRecyclerAdapter(Context context, List<PostsRelated> items, OnRecyclerClickListener listener) {
        mContext = context;
        mValues = items;
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case TYPE_MINOR:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_news_minor, parent, false);
                return new MinorNewsHolder(view);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, int position) {
        PostsRelated item = mValues.get(position);

        switch (getItemViewType(position)) {
            case TYPE_MINOR:
                final MinorNewsHolder minorNewsHolder = (MinorNewsHolder) viewHolder;
                minorNewsHolder.setItem(item);
                minorNewsHolder.mTitleTxt.setText(mValues.get(position).getTitle());
                minorNewsHolder.mDateTxt.setText(mValues.get(position).getCreatedAtString());
                Picasso.with(mContext)
                        .load(item.getImage())
                        .centerInside()
                        .resize(500, 500)
                        .into(minorNewsHolder.mImageView);

                /*minorNewsHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != mListener) {
                            // Notify the active callbacks interface (the activity, if the
                            // fragment is attached to one) that an item has been selected.
                            mListener.onClick(minorNewsHolder.mItem.id);
                        }
                    }
                });*/
                break;
            default:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class MinorNewsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final View mView;
        public final TextView mTitleTxt;
        public final TextView mDateTxt;
        public final ImageView mImageView;
        public PostsRelated mItem;

        public MinorNewsHolder(View view) {
            super(view);
            mView = view;
            mView.setOnClickListener(this);
            mTitleTxt = (TextView) view.findViewById(R.id.titleTxt);
            mDateTxt = (TextView) view.findViewById(R.id.dateTxt);
            mImageView = (ImageView) view.findViewById(R.id.coverImg);
        }

        public void setItem(PostsRelated item) {
            mItem = item;
        }

        @Override
        public void onClick(View v) {
            mListener.onClick(mItem.getId());
        }
    }


    @Override
    public int getItemViewType(int position) {
        return TYPE_MINOR;
    }

    public interface OnRecyclerClickListener {
        void onClick(int postId);
    }
}


class CommentsRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int TYPE_COMMENT = 0;
    public static final int TYPE_REPLY = 1;

    private final List<Comment> mValues;
    private final Context mContext;

    private OnCommentsClickListener mListener;

    public CommentsRecyclerAdapter(List<Comment> items, Context context, OnCommentsClickListener listener) {
        mValues = items;
        mContext = context;
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case TYPE_COMMENT:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_comment, parent, false);
                return new CommentsNewsHolder(view);
            /*case TYPE_REPLY:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_comment_reply, parent, false);
                return new ReplyNewsHolder(view);*/
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, int position) {
        Comment item = mValues.get(position);
        switch (getItemViewType(position)) {
            case TYPE_COMMENT:
                final CommentsNewsHolder commentsNewsHolder = (CommentsNewsHolder) viewHolder;
                commentsNewsHolder.setItem(item);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    commentsNewsHolder.mContainer.setBackgroundColor(mContext.getResources().getColor(R.color.backgroundPrimaryLight, null));
                } else {
                    commentsNewsHolder.mContainer.setBackgroundColor(mContext.getResources().getColor(R.color.backgroundPrimaryLight));
                }
                try {
                    commentsNewsHolder.mUsrNameTxt.setText(item.getUser().getName());
                    Picasso.with(mContext)
                            .load(item.getUser().getImage())
                            .resize(100, 100)
                            .centerCrop()
                            .transform(new CircleTransform())
                            .into(commentsNewsHolder.mImageView);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                commentsNewsHolder.mMessageTxt.setText(item.getMessage());
                commentsNewsHolder.mDateTxt.setText(item.getCreatedAtString());
                commentsNewsHolder.mAnswerBtn.setVisibility(View.GONE);
                break;
            /*case TYPE_REPLY:
                final ReplyNewsHolder replyNewsHolder = (ReplyNewsHolder) viewHolder;
                replyNewsHolder.setItem(item);
                replyNewsHolder.mNameTxt.setText(item.sUser.name);
                replyNewsHolder.mMessageTxt.setText(item.message);
                Picasso.with(mContext)
                        .load(item.sUser.image)
                        .resize(100, 100)
                        .centerCrop()
                        .into(replyNewsHolder.mImageView);
                replyNewsHolder.mDateTxt.setText(item.createdAt);
                replyNewsHolder.mDateTxt.setText(item.createdAt);
                break;*/
            default:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class CommentsNewsHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public final View mView;
        public final LinearLayout mContainer;
        public final TextView mUsrNameTxt;
        public final TextView mSubtitleTxt;
        public final TextView mMessageTxt;
        public final TextView mDateTxt;
        public final ImageView mImageView;
        public final TextView mAnswerBtn;
        private Comment mItem;

        public CommentsNewsHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            mView = view;
            mContainer = (LinearLayout) view.findViewById(R.id.containerLay);
            mUsrNameTxt = (TextView) view.findViewById(R.id.titleTxt);
            mSubtitleTxt = (TextView) view.findViewById(R.id.mainSubtitleTxt);
            mMessageTxt = (TextView) view.findViewById(R.id.descriptionTxt);
            mDateTxt = (TextView) view.findViewById(R.id.dateTxt);
            mImageView = (ImageView) view.findViewById(R.id.coverImg);
            mAnswerBtn = (TextView) view.findViewById(R.id.answer);
        }

        public void setItem(Comment item) {
            mItem = item;
        }

        @Override
        public void onClick(View v) {
            mListener.onClick(mItem.getId());
        }
    }

    /*public class ReplyNewsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final View mView;
        public final TextView mNameTxt;
        public final TextView mMessageTxt;
        public final TextView mDateTxt;
        public final ImageView mImageView;
        private PostMessage mItem;

        public ReplyNewsHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            mView = view;
            mNameTxt = (TextView) view.findViewById(R.id.titleTxt);
            mMessageTxt = (TextView) view.findViewById(R.id.descriptionTxt);
            mDateTxt = (TextView) view.findViewById(R.id.dateTxt);
            mImageView = (ImageView) view.findViewById(R.id.coverImg);
        }

        public void setItem(PostMessage item) {
            mItem = item;
        }

        @Override
        public void onClick(View v) {
            mListener.onClick();
        }
    }*/

    @Override
    public int getItemViewType(int position) {
//        return mValues.get(position).getCommType();
        return 0;
    }

    public interface OnCommentsClickListener {
        void onClick(Integer commentId);
    }
}