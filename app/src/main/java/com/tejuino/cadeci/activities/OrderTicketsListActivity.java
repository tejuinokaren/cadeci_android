package com.tejuino.cadeci.activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.tejuino.cadeci.models.ApiResponse;
import com.tejuino.cadeci.models.Inscription;
import com.tejuino.cadeci.models.Order;
import com.tejuino.cadeci.network.ApiConfig;
import com.tejuino.cadeci.network.ApiSingleton;
import com.tejuino.cadeci.R;
import com.tejuino.cadeci.utils.Repository;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;
import static com.tejuino.cadeci.activities.OrderTicketsListActivity.REQUEST_NEW_TICKET;
import static com.tejuino.cadeci.activities.PaymentMethodsActivity.registerPayment;
import static com.tejuino.cadeci.network.ApiConfig.removeInscription;
import static com.tejuino.cadeci.utils.Repository.formatPriceToMXN;

public class OrderTicketsListActivity extends AppCompatActivity {

    public static final String EXTRA_ORDER = "com.cadeci.order";
    public static final int REQUEST_NEW_TICKET = 100;
    private static final int REQUEST_INVOICE = 101;
    public static final int REQUEST_PAYMENT_DONE = 102;

//    private static boolean sRequiresInvoice;

    private Context mContext;
    private RecyclerView mOrdersRV;
    private List<Inscription> mTicketList = new ArrayList<>();
    private Order mOrder;
    private TicketsListRecyclerAdapter mAdapter;
    private TextView mPriceLabel;
    private static Switch sInvoiceSwitch;
    private Toolbar mToolbar;
    private String mOrderStatus;
    private Button mPaymentBtn;

    public static Intent newIntent(Context packageContext, Order order) {
        Intent i = new Intent(packageContext, OrderTicketsListActivity.class);
        i.putExtra(EXTRA_ORDER, order);
        return i;
    }

    public static boolean getInvoiceStatus() {
        return sInvoiceSwitch.isChecked();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mOrder = (Order) getIntent().getSerializableExtra(EXTRA_ORDER);
        mContext = this;

        setContentView(R.layout.activity_new_order);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.findViewById(R.id.toolbar_back).setVisibility(View.VISIBLE);
        setSupportActionBar(mToolbar);
        sInvoiceSwitch = (Switch) findViewById(R.id.invoiceSwitch);
        mPaymentBtn = (Button) findViewById(R.id.paymentBtn);

        // If is empty then a draft order so immediately open ticketFormActivity,
        // else we are retrieving Inscriptions from a pending or finished order
        if(mOrder != null) {
            mTicketList = mOrder.getInscriptions();
            // check if requested order is a draft
            if (mOrder.getPaymentStatus().equalsIgnoreCase("pendiente") && mOrder.getStatus().equalsIgnoreCase("pendiente")) {
                mOrderStatus = "draft";
                showNewTicketToolbar();
            } else if( mOrder.getPaymentStatus().equalsIgnoreCase("pendiente") && mOrder.getStatus().equalsIgnoreCase("finalizado")) {
                mOrderStatus = "pendiente";
                ((TextView) mToolbar.findViewById(R.id.toolbar_title)).setText("Orden");
//                ((TextView) mToolbar.findViewById(R.id.toolbar_title)).setText("Orden " + formatFolio(mOrder.getId()));
                findViewById(R.id.paymentBtn).setVisibility(View.INVISIBLE);
                findViewById(R.id.referenceBtn).setVisibility(View.VISIBLE);
            } else {
                mOrderStatus = "pagado";
                ((TextView) mToolbar.findViewById(R.id.toolbar_title)).setText("Orden");
//                ((TextView) mToolbar.findViewById(R.id.toolbar_title)).setText("Orden " + formatFolio(mOrder.getId()));
                findViewById(R.id.paymentBtn).setVisibility(View.GONE);
            }
        } else {
            mOrderStatus = "draft";
            disablePaymentButton();
            startActivityForResult(TicketFormActivity.newIntent(mContext, null), REQUEST_NEW_TICKET);
            showNewTicketToolbar();
        }

        mOrdersRV = (RecyclerView) findViewById(R.id.orderRecycler);
        mOrdersRV.setLayoutManager(new LinearLayoutManager(mContext));
        mAdapter = new TicketsListRecyclerAdapter(mContext, mTicketList, mOrderStatus, new TicketsListRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(final Inscription inscription, final int position) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage("¿Deseas eliminar esta inscripción?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                removeInscription(inscription.getId(), position);
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
        mOrdersRV.setAdapter(mAdapter);

        mPriceLabel = (TextView) findViewById(R.id.priceLabel);

        sInvoiceSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    startActivityForResult(InvoiceActivity.newIntent(mContext, mOrder), REQUEST_INVOICE);
                }
            }
        });
    }

    private void enablePaymentButton() {
        mPaymentBtn.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        mPaymentBtn.setEnabled(true);
    }

    private void disablePaymentButton() {
        mPaymentBtn.setBackgroundColor(getResources().getColor(R.color.draftOrderLabelBack));
        mPaymentBtn.setEnabled(false);
    }

    private void showNewTicketToolbar() {
        ((TextView) mToolbar.findViewById(R.id.toolbar_title)).setText(R.string.new_order_title_label);
        ImageButton rightBtn = (ImageButton) mToolbar.findViewById(R.id.toolbar_add);
        rightBtn.setVisibility(View.VISIBLE);
        rightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(TicketFormActivity.newIntent(mContext, null), REQUEST_NEW_TICKET);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mOrder != null) {
            updateTotalPrice();
        }

    }

    private void updateTotalPrice() {
        mPriceLabel.setText(
            formatPriceToMXN((double) mOrder.getTotal())
        );

        if(mOrder.getTotal() > 0 && mOrderStatus.equals("draft")) {
            sInvoiceSwitch.setVisibility(View.VISIBLE);
            enablePaymentButton();
        } else if(mOrder.getInscriptions().size() > 0){
            sInvoiceSwitch.setVisibility(View.GONE);
            enablePaymentButton();
        } else {
            // There aren't any inscriptions in the order
            disablePaymentButton();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
            case R.id.paymentBtn:
                // if order is only populated by inscriptions with coupons, proceed to immediate register
                if(mOrder.getTotal()  > 0) {
                    startActivityForResult(PaymentMethodsActivity.newIntent(mContext, mOrder), REQUEST_PAYMENT_DONE);
                } else {
                    registerPayment(mContext, "cupones", "");
                }
                break;
            case R.id.referenceBtn:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mOrder.getPaymentFile()));
                mContext.startActivity(browserIntent);
                break;
        }
    }

    private void removeInscription(int inscriptionId, final int position) {
        ApiSingleton.getInstance(mContext)
                .addToRequestQueue(new ApiSingleton.GsonRequest(String.format(removeInscription, inscriptionId),
                        ApiResponse.class,
                        Request.Method.POST,
                        ApiConfig.getAuthHeaders(), null, true,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());
                                ApiResponse res = (ApiResponse) response;
                                mTicketList.remove(position);
                                mAdapter.notifyItemRemoved(position);
                                mAdapter.notifyDataSetChanged();
                                mOrder = res.data.order;
                                updateTotalPrice();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                    }
                }));

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_NEW_TICKET) {
            if(resultCode == RESULT_OK) {
                mOrder = (Order) data.getSerializableExtra(EXTRA_ORDER);
                mTicketList.clear();
                mTicketList.addAll(mOrder.getInscriptions());
                mAdapter.notifyDataSetChanged();
            }
        } else if(requestCode == REQUEST_INVOICE) {
            if(resultCode == RESULT_OK) {
                mOrder = (Order) data.getSerializableExtra(EXTRA_ORDER);
            } else if(resultCode == RESULT_CANCELED) {
                sInvoiceSwitch.setChecked(false);
            }
        } else if(requestCode == REQUEST_PAYMENT_DONE) {
            if(resultCode == RESULT_OK) {
                finish();
            }
        }
    }
}

class TicketsListRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private final List<Inscription> mItems;
    private OnItemClickListener mListener;
    private String mOrderStatus = "";

    public TicketsListRecyclerAdapter(Context context, List<Inscription> ticketList, String orderStatus, OnItemClickListener listener) {
        mContext = context;
        mItems = ticketList;
        mOrderStatus = orderStatus;
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_ticket, parent, false);
        return new OrderHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final Inscription item = mItems.get(position);

        final OrderHolder orderHolder = (OrderHolder) holder;
        orderHolder.setItem(item);
        orderHolder.mUsrNameTxt.setText(item.getName() + " " + item.getPaternalSurname() + " " + item.getMaternalSurname());
        orderHolder.mSpecialismTxt.setText(item.getProfession().getTitle());
        orderHolder.mPriceTxt.setText(formatPriceToMXN(item.getTotal()));
        if (mOrderStatus.equalsIgnoreCase("draft")) {
            orderHolder.mDeleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemClick(item, position);
                }
            });
            orderHolder.mEditBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((Activity) mContext).startActivityForResult(TicketFormActivity.newIntent(mContext, mItems.get(position)), REQUEST_NEW_TICKET);
                }
            });
        } else {
            orderHolder.mEditBtn.setVisibility(View.GONE);
            int draw = R.drawable.ic_cash;
            if(item.getCoupon() != null)
                draw = R.drawable.ic_coupon;
            orderHolder.mDeleteBtn.setImageResource(draw);
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class OrderHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final View mView;
        public final TextView mUsrNameTxt;
        public final TextView mSpecialismTxt;
        public final TextView mPriceTxt;
        public final ImageButton mEditBtn;
        public final ImageButton mDeleteBtn;
        private Inscription mItem;

        public OrderHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            mView = view;
            mUsrNameTxt = (TextView) view.findViewById(R.id.nameLabel);
            mSpecialismTxt = (TextView) view.findViewById(R.id.specialismLabel);
            mPriceTxt = (TextView) view.findViewById(R.id.priceLabel);
            mEditBtn = (ImageButton) view.findViewById(R.id.editBtn);
            mDeleteBtn = (ImageButton) view.findViewById(R.id.deleteBtn);
        }

        public void setItem(Inscription item) {
            mItem = item;
        }

        @Override
        public void onClick(View v) {
            if(mItem.getPaymentStatus().equalsIgnoreCase("pagado")) {
                Repository.openPdfURL(mItem.getCodeFile(), mContext);
            } else if(mOrderStatus.equalsIgnoreCase("pendiente")){
                Toast.makeText(mContext, mContext.getResources().getString(R.string.ticket_unpayed), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Inscription inscription, int position);
    }

}

