package com.tejuino.cadeci.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.tejuino.cadeci.models.ApiResponse;
import com.tejuino.cadeci.models.Order;
import com.tejuino.cadeci.models.PaymentMethod;
import com.tejuino.cadeci.network.ApiConfig;
import com.tejuino.cadeci.network.ApiSingleton;
import com.tejuino.cadeci.utils.PaypalHelper;
import com.tejuino.cadeci.R;
import com.tejuino.cadeci.utils.Repository;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.ContentValues.TAG;
import static com.tejuino.cadeci.activities.OrderTicketsListActivity.EXTRA_ORDER;
import static com.tejuino.cadeci.activities.OrderTicketsListActivity.REQUEST_PAYMENT_DONE;

public class PaymentMethodsActivity extends AppCompatActivity {

    public static final String EXTRA_PAYMENT_TYPE_EXTRA = "com.lpv.paymentType";
    private static final int REQUEST_CODE_PAYMENT = 100;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 101;
    private static final int RESULT_EXTRAS_INVALID = 200;
    private static final int REQUEST_PAYMENT_TYPE = 201;
    private String currency = "MXN";
    private String oxxoPDFUrl = "https://www.adobe.com/be_en/active-use/pdf/test.pdf";
    private String oxxoPDFName = "Test Name";

    private Context mContext;
    private PaymentMethodsRecyclerAdapter mAdapter;
    private RecyclerView recyclerView;
    private List<PaymentMethod> paymentList = new ArrayList<>();

    public static Intent newIntent(Context packageContext, Order order) {
        Intent i = new Intent(packageContext, PaymentMethodsActivity.class);
        i.putExtra(EXTRA_ORDER, order);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method);

        mContext = this;
        init();
    }

    private void init() {
        // Toolbar show title with custom textView instead of toolbars normal title text
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Payment Methods");
        toolbar.findViewById(R.id.toolbar_back).setVisibility(View.VISIBLE);

//        paymentList.add(new PaymentMethod(1, "Card", R.drawable.ic_logo_cards));
        paymentList.add(new PaymentMethod(2, "PayPal", R.drawable.ic_logo_paypal));
//        paymentList.add(new PaymentMethod(3, "OXXO", R.drawable.ic_logo_oxxo));
        paymentList.add(new PaymentMethod(4, "Referencia", R.drawable.logo_ixe));
        // Set adapter using jsonarray from response
        mAdapter = new PaymentMethodsRecyclerAdapter(paymentList, mContext);

        // RecyclerView
        recyclerView = (RecyclerView)findViewById(R.id.payment_recycler);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

        mAdapter.setOnItemClickListner(new PaymentMethodsRecyclerAdapter.OnItemClickListner(){

            @Override
            public void OnItemClicked(String paymentType) {
                switch(paymentType) {
                    case "PayPal":
                        payPaypal();
                        break;
                    case "Card":
                        startActivityForResult(CardFormActivity.newIntent(mContext), REQUEST_PAYMENT_DONE);
                        break;
                    case "OXXO":
                        registerPayment(mContext, "oxxo", "oxxo");
                        break;
                    case "Referencia":
                        registerPayment(mContext, "ixe", "ixe");
                        break;
                }
            }
        });
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_add:
                finish();
                break;
            case R.id.toolbar_back:
                finish();
                break;
        }
    }

    public void payPaypal(){
        int price = ((Order) getIntent().getSerializableExtra(EXTRA_ORDER)).getTotal();

        Intent intent = PaypalHelper.createPaymentIntent(PaymentMethodsActivity.this,
                "Congreso CADECI", Double.valueOf(price), currency);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_PAYMENT_DONE) {
            if(resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                finish();
            }
        } else if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.i(TAG, confirm.toJSONObject().toString(4));
                        Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));

                        JSONObject response = confirm.toJSONObject().getJSONObject("response");

                        String paymentId = response.getString("id");
                        String state = response.getString("state");

                        if(state.equals("approved")) {
                            /** TODO: activateCampaign("Paypal", paymentId); */
                            Log.d("tokenId", paymentId);

                            registerPayment(mContext, "paypal", paymentId);

                        }else{
                            Toast.makeText(getBaseContext(), "El cargo fue rechazado",
                                    Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getBaseContext(), "ERROR", Toast.LENGTH_SHORT).show();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "The user canceled.");
            } else if (resultCode == RESULT_EXTRAS_INVALID) {
                Log.i(TAG, "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        } else if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("FuturePaymentExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("FuturePaymentExample", authorization_code);

                        //sendAuthorizationToServer(auth);
                        displayResultText("Future Payment code received from PayPal");

                    } catch (JSONException e) {
                        Log.e("FuturePaymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("FuturePaymentExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "FuturePaymentExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        } else if (requestCode == REQUEST_PAYMENT_TYPE) {
            if (resultCode == Activity.RESULT_OK) {
                switch (data.getStringExtra(EXTRA_PAYMENT_TYPE_EXTRA)) {
                    case "PayPal" :
                        payPaypal();
                        break;
                }
            }
        }
    }

    protected void displayResultText(String result) {
        Toast.makeText(
                getApplicationContext(),
                result, Toast.LENGTH_LONG)
                .show();
    }

    public static void registerPayment(final Context ctx, String method, String token) {
        Map<String, String> params = new HashMap<>();
        params.put("payment_method", method);
        params.put("payment_token", token);
        params.put("invoice_required", OrderTicketsListActivity.getInvoiceStatus() ? "1" : "0");

        // Access the RequestQueue through your singleton class.
        ApiSingleton.getInstance(ctx)
                .addToRequestQueue(new ApiSingleton.GsonRequest(ApiConfig.payment,
                        ApiResponse.class,
                        Request.Method.POST,
                        ApiConfig.getAuthHeaders(), params, true,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());
                                ApiResponse res = (ApiResponse) response;
                                Order order = res.data.order;
                                ((Activity) ctx).startActivityForResult(PurchaseSummary.newIntent(ctx, order), REQUEST_PAYMENT_DONE);

                                if(order.getPaymentType().equalsIgnoreCase("oxxo")
                                        || order.getPaymentType().equalsIgnoreCase("ixe") ) {
                                    Repository.openPdfURL(order.getPaymentFile(), ctx);
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                    }
                }));
    }
}

class PaymentMethodsRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM_PAY_METHOD = 1;

    private List<PaymentMethod> mPaymentList = new ArrayList<>();
    private Context mContext;
    OnItemClickListner onItemClickListner;

    public PaymentMethodsRecyclerAdapter(List<PaymentMethod> payMethods, Context context) {
        mPaymentList = payMethods;
        mContext = context;
    }

    public class PaymentMethodHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private PaymentMethod mItem;
        private ImageView image;

        public PaymentMethodHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            image = (ImageView) v.findViewById(R.id.payment_img);
        }

        public void setItem(PaymentMethod item) {
            mItem = item;
        }

        @Override
        public void onClick(View v) {
            onItemClickListner.OnItemClicked(mItem.name);
        }
    }


    @Override
    public int getItemViewType(int position) {
        //return mUsersList.get(position).sUser != null ? ITEM_USER : ITEM_PAY_METHOD;
        return ITEM_PAY_METHOD;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        switch (viewType) {
            case ITEM_PAY_METHOD:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_payment_method, parent, false);
                return new PaymentMethodHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final int itemType = getItemViewType(position);
        switch (itemType) {
            case ITEM_PAY_METHOD:
                PaymentMethodHolder paymentHolder = (PaymentMethodHolder) holder;
                PaymentMethod payMethod = mPaymentList.get(position);
                paymentHolder.setItem(payMethod);

                paymentHolder.image.setImageResource(payMethod.drawableImg);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mPaymentList.size();
    }

    public interface OnItemClickListner {
        void OnItemClicked(String paymentType);
    }

    public void setOnItemClickListner(OnItemClickListner onItemClickListner) {
        this.onItemClickListner = onItemClickListner;
    }
}