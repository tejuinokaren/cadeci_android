package com.tejuino.cadeci.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tejuino.cadeci.models.Place;
import com.tejuino.cadeci.R;
import com.tejuino.cadeci.views.CircleTransform;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class PlacesSelectorActivity<T> extends AppCompatActivity{

    private static final String EXTRA_DATA_LIST = "com.cadeci.dataList";
    public static final String EXTRA_PLACE = "com.cadeci.place";
    private Context mContext;
    private PlacesRecyclerAdapter mAdapter;

    public static<T> Intent newIntent(Context packageContext, List<T> list) {
        Intent i = new Intent(packageContext, PlacesSelectorActivity.class);
        i.putExtra(EXTRA_DATA_LIST, (Serializable) list);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places);

        mContext = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText("Inscripción");
        setSupportActionBar(toolbar);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.places_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        List<Place> countries = (List<Place>) getIntent().getSerializableExtra(EXTRA_DATA_LIST);
        mAdapter = new PlacesRecyclerAdapter(countries, mContext, new PlacesRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onClick(View v, Place mItem) {
                Intent i = new Intent();
                i.putExtra(EXTRA_PLACE, mItem);
                setResult(RESULT_OK, i);
                finish();
            }
        });
        recyclerView.setAdapter(mAdapter);

        TextView searchTxt = (TextView) findViewById(R.id.search_edit);
        searchTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }
}

/**
 * Adapter for objects with image - text
 */
class PlacesRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private List<Place> mOrigPlacesList = new ArrayList<>();
    private List<Place> mPlacesList = new ArrayList<>();
    private Context mContext;
    private PlacesFilter mPlacesFilter;
    private OnItemClickListener mListener;

    public PlacesRecyclerAdapter(List<Place> users, Context context, OnItemClickListener listener) {
        mOrigPlacesList = users;
        mPlacesList = mOrigPlacesList;
        mContext = context;
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_country, parent, false);

        return new PlaceViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        Place user = mPlacesList.get(position);
        PlaceViewHolder placeHolder = (PlaceViewHolder) holder;
        placeHolder.setItem(user);
        if(user.getImage() != null) {
            Picasso.with(mContext)
                    .load(user.getImage())
                    .resize(150, 150)
                    .centerCrop()
                    .transform(new CircleTransform())
                    .into(placeHolder.image);
        } else {
            placeHolder.image.setVisibility(View.GONE);
        }

        placeHolder.name.setText(user.getTitle());
    }

    public class PlaceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private Place mItem;
        private ImageView image;
        private TextView name, phoneNo;

        public PlaceViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            image = (ImageView) v.findViewById(R.id.countryImg);
            name = (TextView) v.findViewById(R.id.countryName);
        }

        public void setItem(Place item) {
            mItem = item;
        }

        @Override
        public void onClick(View v) {
            mListener.onClick(v, mItem);
        }
    }

    @Override
    public int getItemCount() {
        return mPlacesList.size();
    }

    @Override
    public Filter getFilter() {
        if (mPlacesFilter == null)
            mPlacesFilter = new PlacesFilter();

        return mPlacesFilter;
    }

    private class PlacesFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults result = new FilterResults();
            if(constraint == null || constraint.length() == 0) {
                result.values = mOrigPlacesList;
                result.count = mOrigPlacesList.size();
            } else {
                ArrayList<Place> filteredList = new ArrayList<>();
                for(Place j: mOrigPlacesList) {
                    if (j.getTitle().toUpperCase().startsWith(constraint.toString().toUpperCase()))
                        filteredList.add(j);
                }
                result.values = filteredList;
                result.count = filteredList.size();
            }

            return result;
        }
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mPlacesList = (ArrayList<Place>) results.values; // addAll doesn't work in here
            notifyDataSetChanged();

        }

    }

    public interface OnItemClickListener {
        void onClick(View v, Place mItem);
    }

}

