package com.tejuino.cadeci.activities;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;
import com.tejuino.cadeci.models.ApiResponse;
import com.tejuino.cadeci.models.ProfileItem;
import com.tejuino.cadeci.R;
import com.tejuino.cadeci.views.BorderedCircleTransform;
import com.tejuino.cadeci.network.ApiConfig;
import com.tejuino.cadeci.network.ApiSingleton;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.tejuino.cadeci.utils.Repository.getFieldText;
import static com.tejuino.cadeci.utils.Repository.hideKeyboard;
import static com.tejuino.cadeci.utils.SessionManager.sSession;
import static com.tejuino.cadeci.utils.SessionManager.sUser;

public class ProfileEditActivity extends AppCompatActivity {

    private Context mContext;
    private List<ProfileItem> mProfileItems = new ArrayList<>();
    private EditText mNameField, mLastnameField, mPhoneField, mEmailField;
    private TextView mProfileName;
    private Uri mProfileUri;
    private ImageView mProfileImg;
    private final String TAG = "ProfileEdit";

    public static Intent newIntent(Context packageContext) {
        Intent i = new Intent(packageContext, ProfileEditActivity.class);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        mContext = this;

        init();
    }

    private void init() {
        // Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(R.string.profile_edit_bar_title);
        ImageButton check = (ImageButton) toolbar.findViewById(R.id.toolbar_check);
        check.setVisibility(View.VISIBLE);
        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(ProfileEditActivity.this);
                updateUserData();
            }
        });
        ImageButton cancel = (ImageButton) toolbar.findViewById(R.id.toolbar_cancel);
        cancel.setVisibility(View.VISIBLE);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // Profile Image
        mProfileImg = (ImageView) findViewById(R.id.profileImg);
        Picasso.with(mContext)
                .load(sUser.getInstance().getImage())
                .resize(400, 400)
                .centerInside()
                .transform(new BorderedCircleTransform(10, getResources().getColor(R.color.colorAccent)))
                .into(mProfileImg);
        mProfileImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //carry out the crop operation
                CropImage.activity(null)
                        .setCropShape(CropImageView.CropShape.RECTANGLE)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setAspectRatio(1, 1)
                        .setAllowFlipping(false)
                        .setAllowRotation(false)
                        .setRequestedSize(800, 800)
                        .start(ProfileEditActivity.this);
            }
        });

        // Fields
        mNameField = (EditText) findViewById(R.id.nameField);
        mNameField.setText(sUser.getInstance().getName());
        mLastnameField = (EditText) findViewById(R.id.lastnameField);
        mLastnameField.setText(sUser.getInstance().getLastName());
        mPhoneField = (EditText) findViewById(R.id.phoneField);
        mPhoneField.setText(sUser.getInstance().getPhoneNumber());
        mEmailField = (EditText) findViewById(R.id.emailField);
        mEmailField.setText(sUser.getInstance().getEmail());

        TextView profileEmail = (TextView) findViewById(R.id.profileEmail);
        profileEmail.setText(sUser.getInstance().getEmail());
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check result from CountrySelectorActivity.class
        switch (requestCode) {
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    mProfileUri = result.getUri();
                    //display the returned cropped image
                    Picasso.with(mContext)
                            .load(mProfileUri)
                            .resize(200, 200)
                            .centerCrop()
                            .transform(new BorderedCircleTransform(10, getResources().getColor(R.color.colorAccent)))
                            .into(mProfileImg);
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                }
                break;
        }
    }

    private void updateUserData() {
        HashMap<String, String> headers = ApiConfig.getAuthHeaders();
        Map<String, String> params = getParams();
        if (params == null) {
            return;
        }

        // Access the RequestQueue through your singleton class.
        ApiSingleton.getInstance(mContext)
                .addToRequestQueue(new ApiSingleton.GsonRequest(ApiConfig.userProfile,
                        ApiResponse.class,
                        Request.Method.POST,
                        ApiConfig.getAuthHeaders(), params, true,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());
                                ApiResponse res = (ApiResponse) response;
                                Toast.makeText(mContext, R.string.save_success, Toast.LENGTH_SHORT).show();

                                sUser.setInstance(res.data.user);
                                sSession.createLoginSession();

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                    }
                }));

    }

    private Map<String, String> getParams() {
        HashMap<String, String> params = new HashMap<>();
        boolean allSet = true;
        String errorTxt = getResources().getString(R.string.required_field);
        if (!mNameField.getText().toString().isEmpty()) {
            params.put("name", getFieldText(mNameField));
        } else {
            mNameField.setError(errorTxt);
            allSet = false;
        }

        params.put("last_name", getFieldText(mLastnameField));

        if (mProfileUri != null)
            params.put("image", uploadImages(mContext, mProfileUri));

        params.put("phone", getFieldText(mPhoneField));

        return allSet ? params : null;
    }

    private String uploadImages(Context context, Uri filePath) {
        String imageString = "", imgType;
        imgType = getMimeType(context, filePath);

        try {
            //Getting the Bitmap from Gallery
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), filePath);
            bitmap = scaleToFitMaxSize(bitmap, 400);
            imageString = getBase64StringImage(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "data:" + imgType + ";base64," + imageString;
    }

    public static Bitmap scaleToFitMaxSize(Bitmap b, int maxSide) {
        if (b.getWidth() > maxSide) {
            float factor = maxSide / (float) b.getWidth();
            return Bitmap.createScaledBitmap(b, maxSide, (int) (b.getHeight() * factor), true);
        } else if (b.getHeight() > maxSide) {
            float factor = maxSide / (float) b.getHeight();
            return Bitmap.createScaledBitmap(b, (int) (b.getWidth() * factor), maxSide, true);
        }

        return b;
    }

    public static String getBase64StringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public static String getMimeType(Context context, Uri uriImage) {
        String strMimeType = "";
        Cursor cursor = context.getContentResolver().query(uriImage,
                new String[]{MediaStore.MediaColumns.MIME_TYPE},
                null, null, null);

        if (cursor != null && cursor.moveToNext()) {
            strMimeType = cursor.getString(0);
        }

        if (strMimeType.isEmpty()) {
            String type = null;
            String extension = MimeTypeMap.getFileExtensionFromUrl(String.valueOf(uriImage));
            if (extension != null) {
                type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            }
            return type;
        }

        if (strMimeType.isEmpty()) {
            if (uriImage.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
                ContentResolver cr = context.getContentResolver();
                strMimeType = cr.getType(uriImage);
            } else {
                String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uriImage
                        .toString());
                strMimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                        fileExtension.toLowerCase());
            }
        }
        return strMimeType;
    }

}
