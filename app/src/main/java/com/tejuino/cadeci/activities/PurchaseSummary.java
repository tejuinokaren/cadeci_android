package com.tejuino.cadeci.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tejuino.cadeci.models.Inscription;
import com.tejuino.cadeci.models.Order;
import com.tejuino.cadeci.R;
import com.tejuino.cadeci.utils.Repository;

import java.util.ArrayList;
import java.util.List;

import static com.tejuino.cadeci.activities.OrderTicketsListActivity.EXTRA_ORDER;
import static com.tejuino.cadeci.utils.Repository.formatPriceToMXN;

/**
 * Created by karen on 29/08/17.
 */

public class PurchaseSummary extends AppCompatActivity {

    private PurchaseSummary mContext;
    private RecyclerView mOrdersRV;
    private List<Inscription> mInscriptionList = new ArrayList<>();
    private PurchaseSummaryRecyclerAdapter mAdapter;
    private Order mOrder;

    public static Intent newIntent(Context packageContext, Order order) {
        Intent i = new Intent(packageContext, PurchaseSummary.class);
        i.putExtra(EXTRA_ORDER, order);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_summary);

        mContext = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView)toolbar.findViewById(R.id.toolbar_title)).setText(R.string.purchase_summary_title_label);
        toolbar.findViewById(R.id.toolbar_back).setVisibility(View.VISIBLE);

        mOrder = (Order) getIntent().getSerializableExtra(EXTRA_ORDER);

        mOrdersRV = (RecyclerView) findViewById(R.id.orderRecycler);
        mOrdersRV.setLayoutManager(new LinearLayoutManager(mContext));
        mAdapter = new PurchaseSummaryRecyclerAdapter(mContext, mOrder.getInscriptions(), new PurchaseSummaryRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Inscription inscription, int position) {

            }
        });
        mOrdersRV.setAdapter(mAdapter);

        ((TextView) findViewById(R.id.orderNoLabel)).setText(Repository.formatFolio(mOrder.getId()));
        ((TextView) findViewById(R.id.totalPriceLabel)).setText(formatPriceToMXN((double) mOrder.getTotal()));
        findViewById(R.id.finishBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        super.onBackPressed();
    }
}

class PurchaseSummaryRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private final List<Inscription> mItems;
    private OnItemClickListener mListener;

    public PurchaseSummaryRecyclerAdapter(Context context, List<Inscription> ticketList, OnItemClickListener listener) {
        mContext = context;
        mItems = ticketList;
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_purchase_summary, parent, false);
        return new OrderHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final Inscription item = mItems.get(position);

        final OrderHolder orderHolder = (OrderHolder) holder;
        orderHolder.setItem(item);
        orderHolder.mIndexTxt.setText(String.valueOf(position + 1));
        orderHolder.mNameTxt.setText(item.getName() + " " + item.getPaternalSurname() + " " + item.getMaternalSurname());
        orderHolder.mPriceTxt.setText(Repository.formatPriceToMXN((double) item.getTotal()));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class OrderHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final View mView;
        public final TextView mIndexTxt;
        public final TextView mNameTxt;
        public final TextView mPriceTxt;
        private Inscription mItem;

        public OrderHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            mView = view;
            mIndexTxt = (TextView) view.findViewById(R.id.indexLabel);
            mNameTxt = (TextView) view.findViewById(R.id.nameLabel);
            mPriceTxt = (TextView) view.findViewById(R.id.priceLabel);
        }

        public void setItem(Inscription item) {
            mItem = item;
        }

        @Override
        public void onClick(View v) {
//            mListener.onItemClick(mItem, getAdapterPosition());
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mItem.getCodeFile()));
            mContext.startActivity(browserIntent);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Inscription inscription, int position);
    }
}

