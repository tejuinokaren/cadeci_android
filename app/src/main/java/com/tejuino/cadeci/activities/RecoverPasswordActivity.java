package com.tejuino.cadeci.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.tejuino.cadeci.models.ApiResponse;
import com.tejuino.cadeci.network.ApiConfig;
import com.tejuino.cadeci.network.ApiSingleton;
import com.tejuino.cadeci.R;

import java.util.HashMap;
import java.util.Map;

import static com.tejuino.cadeci.utils.Repository.collapse;
import static com.tejuino.cadeci.utils.Repository.expand;
import static com.tejuino.cadeci.utils.Repository.getFieldText;
import static com.tejuino.cadeci.utils.Repository.isFormCompleted;

public class RecoverPasswordActivity extends AppCompatActivity {

    private static final String TAG = "RecoverPassword";
    private Context mContext;
    private EditText mEmailField;
    private View mView;
    private EditText mRepeatPasswordField, mPasswordField;
    private ImageView mLockImg;
    private TextView mGotCodeLabel;
    private Button mSendBtn, mRecoverBtn;
    private boolean mGotCode;

    public static Intent newIntent(Context packageContext) {
        Intent i = new Intent(packageContext, RecoverPasswordActivity.class);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recover_password);

        mContext = this;
        mView = findViewById(android.R.id.content);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(R.string.recover_bar_title_label);
        toolbar.findViewById(R.id.toolbar_back).setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);

        mPasswordField = (EditText) findViewById(R.id.passwordField);
        mRepeatPasswordField = (EditText) findViewById(R.id.repeatPasswordField);
        mLockImg = (ImageView) findViewById(R.id.recoverImg);
        mGotCodeLabel = (TextView) findViewById(R.id.gotCodeLabel);
        mSendBtn = (Button) findViewById(R.id.sendBtn);
        mRecoverBtn = (Button) findViewById(R.id.recoverBtn);

        mEmailField = (EditText) findViewById(R.id.emailField);
        mEmailField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_DONE){
                    if (isFormCompleted(mView, R.id.formLayout)) {
                        sendRecoverCode();
                    }
                }
                return false;
            }
        });
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
            case R.id.sendBtn:
                if (isFormCompleted(mView, R.id.formLayout)) {
                    sendRecoverCode();
                }
                break;
            case R.id.gotCodeLabel:
                if(!mGotCode) {
                    showGotCodeLayout();
                } else {
                    showNotCodeLayout();
                }
                break;
            case R.id.recoverBtn:
                if (isFormCompleted(mView, R.id.formLayout)) {
                    if (getFieldText(mPasswordField).equals(getFieldText(mRepeatPasswordField))) {
                        recoverPassword();
                    } else {
                        mRepeatPasswordField.setError(getResources().getString(R.string.passwords_must_match));
                    }
                }
                break;
        }
    }

    private void sendRecoverCode() {
        Map<String, String> params = new HashMap<>();
        params.put("email", getFieldText(mEmailField));

        // Access the RequestQueue through your singleton class.
        ApiSingleton.getInstance(mContext)
                .addToRequestQueue(new ApiSingleton.GsonRequest(ApiConfig.recoverPassword,
                        ApiResponse.class,
                        Request.Method.POST,
                        ApiConfig.getUnauthenticatedHeaders(), params, true,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());
                                ApiResponse res = (ApiResponse) response;
                                AlertDialog.Builder builder;
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
                                } else {
                                    builder = new AlertDialog.Builder(mContext);
                                }
                                builder.setMessage(res.data.message)
                                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                                showGotCodeLayout();
                                            }
                                        })
                                        .show();

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                    }
                }));
    }

    private void recoverPassword() {
        Map<String, String> params = new HashMap<>();
        params.put("email", getFieldText(mEmailField));
        params.put("recover_code", getFieldText(mView, R.id.codeField));
        params.put("new_password", getFieldText(mPasswordField));

        // Access the RequestQueue through your singleton class.
        ApiSingleton.getInstance(mContext)
                .addToRequestQueue(new ApiSingleton.GsonRequest(ApiConfig.updatePassword,
                        ApiResponse.class,
                        Request.Method.POST,
                        ApiConfig.getUnauthenticatedHeaders(), params, true,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());
                                ApiResponse res = (ApiResponse) response;
                                Toast.makeText(mContext, res.data.message, Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                    }
                }));
    }

    private void showGotCodeLayout() {
        mGotCode = true;
        mPasswordField.setTag(getResources().getString(R.string.tag_required));
        mRepeatPasswordField.setTag(getResources().getString(R.string.tag_required));
        mGotCodeLabel.setText(R.string.recover_not_code_label);

        mSendBtn.setVisibility(View.GONE);
        mRecoverBtn.setVisibility(View.VISIBLE);

        expand(findViewById(R.id.codeLay));

        mLockImg.setImageResource(R.drawable.ic_unlock);
    }

    private void showNotCodeLayout() {
        mGotCode = false;
        mPasswordField.setTag("");
        mRepeatPasswordField.setTag("");

        mGotCodeLabel.setText(R.string.recover_got_code_label);
        mSendBtn.setVisibility(View.VISIBLE);
        mRecoverBtn.setVisibility(View.GONE);

        collapse(findViewById(R.id.codeLay));

        mLockImg.setImageResource(R.drawable.ic_lock);
    }
}
