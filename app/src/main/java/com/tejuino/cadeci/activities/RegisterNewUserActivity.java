package com.tejuino.cadeci.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.tejuino.cadeci.models.ApiResponse;
import com.tejuino.cadeci.R;
import com.tejuino.cadeci.network.ApiConfig;
import com.tejuino.cadeci.network.ApiSingleton;

import java.util.HashMap;

import static com.tejuino.cadeci.utils.Repository.getFieldText;
import static com.tejuino.cadeci.utils.Repository.isFormCompleted;
import static com.tejuino.cadeci.utils.SessionManager.sSession;
import static com.tejuino.cadeci.utils.SessionManager.sUser;

public class RegisterNewUserActivity extends AppCompatActivity {

    private static final String TAG = "com.cadeci.Register";
    private Context mContext;
    private View mView;

    public static Intent newIntent(Context packageContext) {
        Intent i = new Intent(packageContext, RegisterNewUserActivity.class);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_new_user);
        mContext = this;
        mView = findViewById(android.R.id.content);
        init();
    }

    private void init() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(R.string.register_bar_title_label);
        toolbar.findViewById(R.id.toolbar_back).setVisibility(View.VISIBLE);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.registerBtn:
                if (isFormCompleted(getWindow().getDecorView().getRootView(), R.id.formLayout)) {
                    if (getFieldText(mView, R.id.passwordField).equals(getFieldText(mView, R.id.repeatPasswordField))) {
                        registerUser();
                    } else {
                        ((EditText) findViewById(R.id.repeatPasswordField)).setError(getResources().getString(R.string.passwords_must_match));
                    }
                }
                break;
            case R.id.toolbar_back:
                finish();
                break;
        }
    }

    private void registerUser() {
        HashMap<String, String> params = new HashMap<>();
        params.put("first_name", getFieldText(mView, R.id.nameField));
        params.put("last_name", getFieldText(mView, R.id.lastnameField));
        params.put("email", getFieldText(mView, R.id.emailField));
        params.put("password", getFieldText(mView, R.id.passwordField));

        // Access the RequestQueue through your singleton class.
        ApiSingleton.getInstance(mContext)
                .addToRequestQueue(new ApiSingleton.GsonRequest(ApiConfig.registerUser,
                        ApiResponse.class,
                        Request.Method.POST,
                        ApiConfig.getUnauthenticatedHeaders(), params, true,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());
                                ApiResponse res = (ApiResponse) response;
                                if (res.data.message.equalsIgnoreCase("success")) {
                                    sUser.getInstance().setEmail(getFieldText(mView, R.id.emailField));
                                    sUser.getInstance().setPassword(getFieldText(mView, R.id.passwordField));
                                    sSession.auth(mContext);
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                    }
                }));

    }
}
