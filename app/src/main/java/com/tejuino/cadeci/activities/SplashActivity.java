package com.tejuino.cadeci.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.tejuino.cadeci.utils.SessionManager;

/**
 * Created by karen on 21/06/17.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * If sUser is NOT loggedIn start LoginActivity
         *
         * If IS loggedIn, check if sAuthToken.getExpiresAtDate() value is greater or equals to current date,
         * if so, then refreshToken immediately and wait for callback to start MainActivity;
         * but if value is less than the current date, program a refresh in the future which only updates the
         * token without a callback, an init MainActivity immediately.
         */
        SessionManager.setSplashFlag(true);
        SessionManager.getInstance(this).checkLogin();
    }
}