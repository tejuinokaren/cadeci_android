package com.tejuino.cadeci.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.tejuino.cadeci.models.ApiResponse;
import com.tejuino.cadeci.models.City;
import com.tejuino.cadeci.models.Country;
import com.tejuino.cadeci.models.Inscription;
import com.tejuino.cadeci.models.Place;
import com.tejuino.cadeci.models.Profession;
import com.tejuino.cadeci.models.State;
import com.tejuino.cadeci.network.ApiConfig;
import com.tejuino.cadeci.network.ApiSingleton;
import com.tejuino.cadeci.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.tejuino.cadeci.activities.PlacesSelectorActivity.EXTRA_PLACE;
import static com.tejuino.cadeci.utils.Repository.getFieldText;
import static com.tejuino.cadeci.utils.Repository.isFormCompleted;
import static com.tejuino.cadeci.utils.Repository.isValidEmail;
import static com.tejuino.cadeci.utils.Repository.setAllTextInputLayoutAnim;

public class TicketFormActivity extends AppCompatActivity {

    public static final String EXTRA_TICKET = "com.cadeci.mTicket";
    private static final String TAG = "NewTicket";
    private static final int REQUEST_PLACE = 0;
    private static final int ID_MEXICO = 135;
    private Context mContext;
    private TextView mSpecialismLabel;
    private EditText mNameField, mPaternalSurField, mMaternalSurField, mEmailField, mPhoneField, mCertNameField, mScholarshipField;
    private static List<Profession> sSpecializationsList = new ArrayList<>();
    private static List<Country> sCountriesList = new ArrayList<>();
    private static List<State> sStatesList = new ArrayList<>();
    private static List<City> sCitiesList = new ArrayList<>();
    private Profession mSpecialization;
    private Country mCountry;
    private State mState;
    private City mCity;
    private String mTicketId = "";
    private TextView mCountryLabel, mStateLabel, mCityLabel;
    private SpecializationsSpinAdapter mSpecializationsSpinAdapter;
    private Inscription mTicket;
    private View mView;
    private Spinner mSpecializationsSpinner;

    public static Intent newIntent(Context packageContext, Inscription ticket) {
        Intent i = new Intent(packageContext, TicketFormActivity.class);
        i.putExtra(EXTRA_TICKET, ticket);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_ticket_form);

        mContext = this;
        mView = getWindow().getDecorView().getRootView();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(R.string.new_ticket_bar_title_label);
        setSupportActionBar(toolbar);
        toolbar.findViewById(R.id.toolbar_back).setVisibility(View.VISIBLE);

        mNameField = (EditText) findViewById(R.id.nameField);
        mPaternalSurField = (EditText) findViewById(R.id.paternalSurField);
        mMaternalSurField = (EditText) findViewById(R.id.maternalSurField);
        mEmailField = (EditText) findViewById(R.id.emailField);
        mPhoneField = (EditText) findViewById(R.id.phoneField);
        mCertNameField = (EditText) findViewById(R.id.certNameField);
        mScholarshipField = (EditText) findViewById(R.id.scholarshipField);
        mCountryLabel = (TextView) findViewById(R.id.countryLabel);
        mStateLabel = (TextView) findViewById(R.id.stateLabel);
        mCityLabel = (TextView) findViewById(R.id.cityLabel);
        mSpecialismLabel = (TextView) findViewById(R.id.specialismLabel);

        // Manage Specializations spiner data and click
        mSpecializationsSpinner = (Spinner) findViewById(R.id.specialismSpinner);
        mSpecializationsSpinAdapter = new SpecializationsSpinAdapter(this, android.R.id.text1, sSpecializationsList);
        mSpecializationsSpinAdapter.setDropDownViewResource(R.layout.item_spinner_text);
        mSpecializationsSpinner.setAdapter(mSpecializationsSpinAdapter);
        mSpecializationsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mSpecialization = mSpecializationsSpinAdapter.getItem(position);
                mSpecialismLabel.setText(mSpecializationsSpinAdapter.getItem(position).getTitle());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
//                Toast.makeText(mContext, "Nothing happens", Toast.LENGTH_SHORT).show();
            }
        });

        init();
    }

    private void init() {
        // Load Ticket data
        mTicket = (Inscription) getIntent().getSerializableExtra(EXTRA_TICKET);
        if (mTicket != null) {
            // Disable textInputLayout animation while loading data in editTexts
            // this to enhance layout loading view
            setAllTextInputLayoutAnim(mView, R.id.formLayout, false);

            mTicketId = String.valueOf(mTicket.getId());
            mNameField.setText(mTicket.getName());
            mPaternalSurField.setText(mTicket.getPaternalSurname());
            mMaternalSurField.setText(mTicket.getMaternalSurname());
            mEmailField.setText(mTicket.getEmail());
            mPhoneField.setText(mTicket.getPhone());
            mCertNameField.setText(mTicket.getCertificationName());
            if(mTicket.getCoupon() != null) {
                mScholarshipField.setEnabled(false);
                mScholarshipField.setFocusable(false);
                mScholarshipField.setTextColor(getResources().getColor(R.color.textPrimaryLight));
                mScholarshipField.setText(mTicket.getCoupon().getCode());
            }

            setSpecialization();

            mCountry = mTicket.getCountry();
            updateLayoutWithCountry(mCountry);
            mState = mTicket.getState();
            mCity = mTicket.getCity();
            mStateLabel.setText(mState.getTitle());
            mCityLabel.setText(mCity.getTitle());
            loadCities(mState.getId());
        } else {
            // If there's no ticket data available then show initial places state
            if(!sCountriesList.isEmpty() && !sStatesList.isEmpty() && !sCitiesList.isEmpty()) {
                setMainCountry();
                mState = sStatesList.get(0);
                mStateLabel.setText(mState.getTitle());
                loadCities(mState.getId());
            }
        }

        // Get Specilizations from server just once
        if(sSpecializationsList.isEmpty()) {
            loadSpecializations();
        }
        if(sCountriesList.isEmpty()) {
            loadCountries();
        }
        if(sStatesList.isEmpty()) {
            loadStates();
        }
    }

    private void setSpecialization() {
        for(int i = 0; i<sSpecializationsList.size(); i++) {
            int spId = sSpecializationsList.get(i).getId();
            int tSpId = mTicket.getProfessionId();
            if(spId == tSpId) {
                mSpecializationsSpinner.setSelection(i);
            }
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.countryLay:
                startActivityForResult(PlacesSelectorActivity.newIntent(mContext, sCountriesList), REQUEST_PLACE);
                break;
            case R.id.stateLay:
                startActivityForResult(PlacesSelectorActivity.newIntent(mContext, sStatesList), REQUEST_PLACE);
                break;
            case R.id.cityLay:
                startActivityForResult(PlacesSelectorActivity.newIntent(mContext, sCitiesList), REQUEST_PLACE);
                break;
            case R.id.finishOrderBtn:
                if (isFormCompleted(mView, R.id.formLayout)) {
                    if(isValidEmail(mView, mEmailField)) {
                        addInscription();
                    }
                }
                break;
            case R.id.saveBtn:
                if (isFormCompleted(getWindow().getDecorView().getRootView(), R.id.formLayout)) {
                    if(isValidEmail(mView, mEmailField)) {
                        addInscription();
                    }
                }
                break;
            case R.id.toolbar_back:
                finish();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    private void loadSpecializations() {
        ApiSingleton.getInstance(mContext)
                .addToRequestQueue(new ApiSingleton.GsonRequest(ApiConfig.orderSpecializations,
                        ApiResponse.class,
                        Request.Method.GET,
                        ApiConfig.getAuthHeaders(), null, false,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());
                                ApiResponse res = (ApiResponse) response;
                                sSpecializationsList.clear();
                                sSpecializationsList.addAll(res.data.professions);
                                mSpecializationsSpinAdapter.notifyDataSetChanged();
                                if(mTicket !=  null) {
                                    setSpecialization();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                    }
                }));

    }

    private void loadCountries() {
        ApiSingleton.getInstance(mContext)
                .addToRequestQueue(new ApiSingleton.GsonRequest(ApiConfig.countries,
                        ApiResponse.class,
                        Request.Method.GET,
                        ApiConfig.getAuthHeaders(), null, false,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());
                                ApiResponse res = (ApiResponse) response;
                                sCountriesList.clear();
                                sCountriesList.addAll(res.data.countries);

                                if(mTicket == null) {
                                    setMainCountry();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                    }
                }));

    }

    private void loadStates() {
        ApiSingleton.getInstance(mContext)
                .addToRequestQueue(new ApiSingleton.GsonRequest(String.format(ApiConfig.statesByCountryId, ID_MEXICO), // Load Mexico
                        ApiResponse.class,
                        Request.Method.GET,
                        ApiConfig.getAuthHeaders(), null, false,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());
                                ApiResponse res = (ApiResponse) response;
                                sStatesList.clear();
                                sStatesList.addAll(res.data.states);

                                if(mTicket == null) {
                                    mState = sStatesList.get(0);
                                    mStateLabel.setText(mState.getTitle());
                                    loadCities(mState.getId());
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                    }
                }));

    }

    private void loadCities(int stateId) {
        ApiSingleton.getInstance(mContext)
                .addToRequestQueue(new ApiSingleton.GsonRequest(String.format(ApiConfig.citiesBystateId, stateId),
                        ApiResponse.class,
                        Request.Method.GET,
                        ApiConfig.getAuthHeaders(), null, false,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());
                                ApiResponse res = (ApiResponse) response;
                                sCitiesList.clear();
                                sCitiesList.addAll(res.data.cities);
                                if(mTicket == null) {
                                    mCity = sCitiesList.get(0);
                                    mCityLabel.setText(mCity.getTitle());
                                } else {
                                    // Enable textInputLayout animation
                                    setAllTextInputLayoutAnim(mView, R.id.formLayout, true);
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                    }
                }));

    }

    private void addInscription() {
        Map<String, String> params = new HashMap<>();
        params.put("id", mTicketId);
        params.put("promo_code", getFieldText(mScholarshipField));
        params.put("name", getFieldText(mNameField));
        params.put("paternal_surname", getFieldText(mPaternalSurField));
        params.put("maternal_surname", getFieldText(mMaternalSurField));
        params.put("profession_id", String.valueOf(mSpecialization.getId()));
        params.put("email", getFieldText(mEmailField));
        params.put("phone", getFieldText(mPhoneField));
        params.put("certification_name", getFieldText(mCertNameField));
        params.put("country", String.valueOf(mCountry.getId()));
        params.put("state", mCountry.isMexico() ? String.valueOf(mState.getId()) : "0");
        params.put("city", mCountry.isMexico() ? String.valueOf(mCity.getId()) : "0");
        ApiSingleton.getInstance(mContext)
                .addToRequestQueue(new ApiSingleton.GsonRequest(ApiConfig.addInscription,
                        ApiResponse.class,
                        Request.Method.POST,
                        ApiConfig.getAuthHeaders(), params, true,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());
                                ApiResponse res = (ApiResponse) response;
                                Intent i = new Intent(TicketFormActivity.this, OrderTicketsListActivity.class);
                                i.putExtra(OrderTicketsListActivity.EXTRA_ORDER, res.data.order);
                                setResult(RESULT_OK, i);
                                finish();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                    }
                }));

    }

    private void setMainCountry() {
        for (Country country : sCountriesList) {
            if (country.isMexico()) {
                mCountry = country;
            }
        }

        updateLayoutWithCountry(mCountry);
    }

    /**
     * If selected country is Mexico(id = 135) then load states else hide states and cities
     * @param cty
     */
    private void updateLayoutWithCountry(Country cty) {
        int status = View.GONE;
        if(cty.isMexico()) {
            status = View.VISIBLE;
        }

        mCountryLabel.setText(mCountry.getTitle());
        findViewById(R.id.stateLay).setVisibility(status);
        findViewById(R.id.cityLay).setVisibility(status);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_PLACE:
                if(resultCode == RESULT_OK) {
                    Place place = (Place)data.getSerializableExtra(EXTRA_PLACE);
                    if( place instanceof Country) {
                        mCountry = (Country) place;
                        mCountryLabel.setText(place.getTitle());
                        updateLayoutWithCountry((Country) place);
                    } else if( place instanceof State ) {
                        mState = (State) place;
                        mStateLabel.setText(place.getTitle());
                        loadCities(mState.getId());
                    } else if( place instanceof City ) {
                        mCity = (City) place;
                        mCityLabel.setText(place.getTitle());
                    }
                }
                break;
        }
    }
}

class SpecializationsSpinAdapter extends ArrayAdapter<Profession>{

    // Your sent mContext
    private Context mContext;
    // Your custom values for the mSpecializationsSpinner (User)
    private List<Profession> values;

    public SpecializationsSpinAdapter(Context context, int textViewResourceId,
                                      List<Profession> values) {
        super(context, textViewResourceId, values);
        this.mContext = context;
        this.values = values;
    }

    public int getCount(){
        return values.size();
    }

    public Profession getItem(int position){
        return values.get(position);
    }

    public long getItemId(int position){
        return position;
    }


    // And the "magic" goes here
    // This is for the "passive" state of the mSpecializationsSpinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // I created a dynamic TextView here, but you can reference your own  custom layout for each mSpecializationsSpinner item
        TextView label = new TextView(mContext);
        label.setTextColor(mContext.getResources().getColor(R.color.textPrimaryDark));
        label.setTextSize(18);
        // Then you can get the current item using the values array (Users array) and the current position
        // You can NOW reference each method you has created in your bean object (User class)
        label.setText(values.get(position).getTitle());

        // And finally return your dynamic (or custom) mView for each mSpecializationsSpinner item
        return label;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same mView, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_spinner_text, parent, false);
        TextView label = (TextView) v.findViewById(R.id.titleTxt);
        label.setText(values.get(position).getTitle());

        return label;
    }
}
