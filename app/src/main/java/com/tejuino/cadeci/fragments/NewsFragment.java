package com.tejuino.cadeci.fragments;

/**
 * Created by dianakarenms on 11/04/17.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;
import com.tejuino.cadeci.activities.MainActivity;
import com.tejuino.cadeci.activities.NewsDetailActivity;
import com.tejuino.cadeci.models.ApiResponse;
import com.tejuino.cadeci.models.Posts;
import com.tejuino.cadeci.models.PostsPage;
import com.tejuino.cadeci.network.ApiConfig;
import com.tejuino.cadeci.network.ApiSingleton;
import com.tejuino.cadeci.R;
import com.tejuino.cadeci.utils.Repository;

import java.util.ArrayList;
import java.util.List;

import static com.android.volley.VolleyLog.TAG;
import static com.tejuino.cadeci.utils.Repository.animCrossfade;

public class NewsFragment extends Fragment {

    private static final String ARG_COLUMN_COUNT = "column-count";

    private int mFirstPage = 1;
    private int mCurrentPage = 1;
    private boolean mIsLastPage;
    private NewsRecyclerAdapter.OnRecyclerClickListener mListener;
    private Context mContext;
    private List<Posts> mNewsList = new ArrayList<>();
    private NewsRecyclerAdapter mAdapter;
    private Repository.PaginationRefreshRecycler mPaginationRefreshRecycler;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public NewsFragment() {
    }

    @SuppressWarnings("unused")
    public static NewsFragment newInstance(int columnCount) {
        NewsFragment fragment = new NewsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();

        // Empty scheduleTab
        if(mNewsList.isEmpty()) {
            for (int i = 1; i <= 2; i++)
                mNewsList.add(new Posts());
        }

        mAdapter = new NewsRecyclerAdapter(mContext, mNewsList, mListener);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_news_list, container, false);

        animCrossfade(v.findViewById(R.id.container));
        //((Activity) getContext()).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

        // Set toolbar title
        ((MainActivity)getActivity()).setToolbarTitle(getResources().getString(R.string.news_bar_title_label));
        ((MainActivity)getActivity()).getToolbar().setVisibility(View.VISIBLE);

        // Set the adapter
        Context context = v.getContext();

        mPaginationRefreshRecycler = (Repository.PaginationRefreshRecycler) v.findViewById(R.id.list);
        mPaginationRefreshRecycler.setLayoutManager(new LinearLayoutManager(context));
        mPaginationRefreshRecycler.setAdapter(mAdapter);
        mPaginationRefreshRecycler.setList(mNewsList)
                .setPaginationCurrentPage(mCurrentPage, mIsLastPage)
                .addPagination(10, new Posts().setType(Posts.TAG_PROGRESS), new Repository.PaginationRefreshRecycler.PaginationListener() {
                    @Override
                    public void onLoadNextPage(int pageNumber) {
                        mCurrentPage = pageNumber;
                        loadNewsPosts(pageNumber);
                    }
                })
                .addSwipeRefresh((SwipeRefreshLayout) v.findViewById(R.id.swiperefresh), new Repository.PaginationRefreshRecycler.SwipeRefreshListener() {
                    @Override
                    public void onRequestData() {
                        mIsLastPage = false;
                        loadNewsPosts(mFirstPage);
                    }
                });

        if(mNewsList.get(0).getType().equals(Posts.TAG_DUMMY)) {
            loadNewsPosts(mFirstPage);
        }
        return v;
    }

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        mListener = new NewsRecyclerAdapter.OnRecyclerClickListener() {
            @Override
            public void onClick(int postId, ImageView coverImg) {
                /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Intent intent = NewsDetailActivity.newIntent(mContext, postId);
                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeSceneTransitionAnimation((Activity) context, coverImg, "news_transition");
                    startActivity(intent, options.toBundle());
                } else {*/
                    startActivity(NewsDetailActivity.newIntent(mContext, postId));
//                }
            }
        };
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void loadNewsPosts(int pageNumber) {
        // Access the RequestQueue through your singleton class.
        ApiSingleton.getInstance(mContext)
                .addToRequestQueue(new ApiSingleton.GsonRequest(ApiConfig.posts + "?page=" + pageNumber,
                        ApiResponse.class,
                        Request.Method.GET,
                        ApiConfig.getUnauthenticatedHeaders(), null, false,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());
                                ApiResponse res = (ApiResponse) response;

                                PostsPage page = res.data.mPostsPage;

                                if(!mNewsList.isEmpty()) {
                                    // Remove dummy items shown during first data loading
                                    if (mNewsList.get(0).getType().equals(Posts.TAG_DUMMY)) {
                                        mNewsList.clear();
                                    }
                                }

                                // As recycler is created each time the fragment becomes visible
                                // we need to locally store if lastPage has been reached to prevent
                                // trying to load a non existent next page.
                                if(page.getCurrentPage() == page.getLastPage()) {
                                    mIsLastPage = true;
                                }

                                mPaginationRefreshRecycler.paginationLoadComplete(page.getLastPage(), page.getPostsList());

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                        mPaginationRefreshRecycler.stopRefresh();
                    }
                }));

    }
}

/**
 * {@link RecyclerView.Adapter} that can display a {@link Posts} and makes a call to the
 * specified {@link OnRecyclerClickListener}.
 */
class NewsRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int TYPE_DUMMY_MAIN = -2;
    public static final int TYPE_DUMMY_MINOR = -1;
    public static final int TYPE_MAIN = 0;
    public static final int TYPE_SECONDARY = 1;
    public static final int TYPE_MINOR = 2;
    private static final int TYPE_PROGRESS = 3;

    private List<Posts> mValues;
    private OnRecyclerClickListener mListener;
    private Context mContext;

    public NewsRecyclerAdapter(Context context, List<Posts> items, OnRecyclerClickListener listener) {
        mContext = context;
        mValues = items;
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case TYPE_MAIN:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_news_main, parent, false);
                return new MainNewsHolder(view);
            case TYPE_SECONDARY:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_news_secondary, parent, false);
                return new SecondaryNewsHolder(view);
            case TYPE_MINOR:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_news_minor, parent, false);
                return new MinorNewsHolder(view);
            case TYPE_DUMMY_MAIN:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_news_main, parent, false);
                return new MainNewsHolder(view);
            case TYPE_DUMMY_MINOR:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_news_minor, parent, false);
                return new MinorNewsHolder(view);
            case TYPE_PROGRESS:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_progress_view, parent, false);
                return new ProgressViewHolder(view);
            default:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_progress_view, parent, false);
                return new ProgressViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, int position) {
        Posts item = mValues.get(position);
        int backColor = mContext.getResources().getColor(R.color.dummiesBack);

        switch (getItemViewType(position)) {
            case TYPE_MAIN:
                final MainNewsHolder mainNewsHolder = (MainNewsHolder) viewHolder;
                mainNewsHolder.setItem(mValues.get(position));
                mainNewsHolder.mTitleTxt.setText(mValues.get(position).getTitle());
                mainNewsHolder.mDateTxt.setText(mValues.get(position).getCreatedAtString());
                mainNewsHolder.mDescriptionTxt.setText(mValues.get(position).getExtract());
                Picasso.with(mContext)
                        .load(item.getImage())
                        .centerInside()
                        .resize(500, 500)
                        .into(mainNewsHolder.mImageView);

                mainNewsHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != mListener) {
                            // Notify the active callbacks interface (the activity, if the
                            // fragment is attached to one) that an item has been selected.
                            mListener.onClick(mainNewsHolder.mItem.getId(), mainNewsHolder.mImageView);
                        }
                    }
                });
                break;
            case TYPE_SECONDARY:
                final SecondaryNewsHolder secondaryNewsHolder = (SecondaryNewsHolder) viewHolder;
                secondaryNewsHolder.setItem(mValues.get(position));
                secondaryNewsHolder.mTitleTxt.setText(mValues.get(position).getTitle());
                secondaryNewsHolder.mDescriptionTxt.setText(mValues.get(position).getExtract());
                Picasso.with(mContext)
                        .load(item.getImage())
                        .centerInside()
                        .resize(500, 500)
                        .into(secondaryNewsHolder.mImageView);
//                secondaryNewsHolder.mDateTxt.setText(mValues.get(position).createdAt);


                secondaryNewsHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != mListener) {
                            // Notify the active callbacks interface (the activity, if the
                            // fragment is attached to one) that an item has been selected.
                            mListener.onClick(secondaryNewsHolder.mItem.getId(), secondaryNewsHolder.mImageView);
                        }
                    }
                });
                break;
            case TYPE_MINOR:
                final MinorNewsHolder minorNewsHolder = (MinorNewsHolder) viewHolder;
                minorNewsHolder.setItem(mValues.get(position));
                minorNewsHolder.mTitleTxt.setText(mValues.get(position).getTitle());
                minorNewsHolder.mDateTxt.setText(mValues.get(position).getCreatedAtString());
                Picasso.with(mContext)
                        .load(item.getImage())
                        .centerCrop()
                        .resize(500, 500)
                        .into(minorNewsHolder.mImageView);

                minorNewsHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != mListener) {
                            // Notify the active callbacks interface (the activity, if the
                            // fragment is attached to one) that an item has been selected.
                            mListener.onClick(minorNewsHolder.mItem.getId(), minorNewsHolder.mImageView);
                        }
                    }
                });
                break;
            case TYPE_DUMMY_MAIN:
                final MainNewsHolder dummyMainHolder = (MainNewsHolder) viewHolder;
                dummyMainHolder.mTitleTxt.setBackgroundColor(backColor);
                dummyMainHolder.mDateTxt.setBackgroundColor(backColor);
                dummyMainHolder.mDescriptionTxt.setBackgroundColor(backColor);
                dummyMainHolder.mImageView.setBackgroundColor(backColor);
                break;
            case TYPE_DUMMY_MINOR:
                final MinorNewsHolder dummySchHolder = (MinorNewsHolder) viewHolder;
                dummySchHolder.mTitleTxt.setBackgroundColor(backColor);
                dummySchHolder.mDateTxt.setBackgroundColor(backColor);
                dummySchHolder.mImageView.setBackgroundColor(backColor);
                break;
            case TYPE_PROGRESS:
                final ProgressViewHolder progHolder = (ProgressViewHolder) viewHolder;
                progHolder.progressBar.setIndeterminate(true);
                break;
            default:
                final ProgressViewHolder holder = (ProgressViewHolder) viewHolder;
                holder.progressBar.setIndeterminate(true);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class MainNewsHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTitleTxt;
        public final TextView mDescriptionTxt;
        public final ImageView mImageView;
        public final TextView mDateTxt;
        public final LinearLayout mBackgroundLay;
        private Posts mItem;

        public MainNewsHolder (View view) {
            super(view);
            mView = view;
            mTitleTxt = (TextView) view.findViewById(R.id.titleTxt);
            mDescriptionTxt = (TextView) view.findViewById(R.id.descriptionTxt);
            mImageView = (ImageView) view.findViewById(R.id.coverImg);
            mDateTxt = (TextView) view.findViewById(R.id.dateTxt);
            mBackgroundLay = (LinearLayout) view.findViewById(R.id.backgroundLay);
        }

        public void setItem(Posts item) {
            mItem = item;
        }
    }

    public class SecondaryNewsHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTitleTxt;
        public final TextView mSubtitleTxt;
        public final TextView mDescriptionTxt;
        public final ImageView mImageView;
        public final TextView mDateTxt;
        private Posts mItem;

        public SecondaryNewsHolder (View view) {
            super(view);
            mView = view;
            mTitleTxt = (TextView) view.findViewById(R.id.titleTxt);
            mSubtitleTxt = (TextView) view.findViewById(R.id.mainSubtitleTxt);
            mDescriptionTxt = (TextView) view.findViewById(R.id.descriptionTxt);
            mImageView = (ImageView) view.findViewById(R.id.coverImg);
            mDateTxt = (TextView) view.findViewById(R.id.dateTxt);
        }

        public void setItem(Posts item) {
            mItem = item;
        }
    }

    public class MinorNewsHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTitleTxt;
        public final TextView mDateTxt;
        public final ImageView mImageView;
        public Posts mItem;

        public MinorNewsHolder(View view) {
            super(view);
            mView = view;
            mTitleTxt = (TextView) view.findViewById(R.id.titleTxt);
            mDateTxt = (TextView) view.findViewById(R.id.dateTxt);
            mImageView = (ImageView) view.findViewById(R.id.coverImg);
        }

        public void setItem(Posts item) {
            mItem = item;
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(mValues.get(position).getType().equals(Posts.TAG_MAIN) || position == 0)  {
            if(!mValues.get(position).getType().equals(Posts.TAG_DUMMY)) {
                if (mValues.get(position).getOutstanding() == 1 || position == 0) {
                    return TYPE_MAIN;
                } else {
                    return TYPE_SECONDARY;
                }
            } else {
                return TYPE_DUMMY_MAIN;
            }
        } else if(mValues.get(position).getType().equals(Posts.TAG_SECONDARY)) {
            return TYPE_MINOR;
        } else if(mValues.get(position).getType().equals(Posts.TAG_DUMMY)) {
            return TYPE_DUMMY_MINOR;
        } else {
            return TYPE_PROGRESS;
        }
    }

    public interface OnRecyclerClickListener {
        void onClick(int postId, ImageView coverImg);
    }
}

