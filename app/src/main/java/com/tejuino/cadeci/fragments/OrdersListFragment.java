package com.tejuino.cadeci.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.tejuino.cadeci.activities.MainActivity;
import com.tejuino.cadeci.activities.OrderTicketsListActivity;
import com.tejuino.cadeci.models.ApiResponse;
import com.tejuino.cadeci.models.Order;
import com.tejuino.cadeci.network.ApiConfig;
import com.tejuino.cadeci.network.ApiSingleton;
import com.tejuino.cadeci.R;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;
import static com.tejuino.cadeci.utils.Repository.animCrossfade;
import static com.tejuino.cadeci.utils.Repository.formatFolio;

/**
 * Created by karen on 10/08/17.
 */

public class OrdersListFragment extends Fragment {
    private RecyclerView mSponsorsRV;
    private Context mContext;
    private OrdersListRecyclerAdapter mAdapter;
    private List<Order> mOrderList = new ArrayList<>();
    private View v;
    private Toolbar mToolbar;

    public static OrdersListFragment newInstance() {
        OrdersListFragment fragment = new OrdersListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        mContext = getContext();

        mAdapter = new OrdersListRecyclerAdapter(mContext, mOrderList, new OrdersListRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onClick(Order item) {
                startActivity(OrderTicketsListActivity.newIntent(mContext, item));
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_tickets, container, false);

        animCrossfade(v.findViewById(R.id.container));

        // Set mToolbar title
        mToolbar = ((MainActivity)getActivity()).getToolbar();
        ((MainActivity)getActivity()).setToolbarTitle(getResources().getString(R.string.tickets_my_orders));
        mToolbar.setVisibility(View.VISIBLE);
        mToolbar.findViewById(R.id.toolbar_add).setVisibility(View.VISIBLE);
        mToolbar.findViewById(R.id.toolbar_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Order firstOrder = mOrderList.get(0);
                    if (!(firstOrder.getPaymentStatus().equalsIgnoreCase("pendiente") && firstOrder.getStatus().equalsIgnoreCase("pendiente"))) {
                        firstOrder = null;
                    }

                    startActivity(OrderTicketsListActivity.newIntent(mContext, firstOrder));
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }
        });

        Button buyBtn = (Button) v.findViewById(R.id.buyTicketBtn);
        buyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(OrderTicketsListActivity.newIntent(mContext, null));
            }
        });

        mSponsorsRV = (RecyclerView) v.findViewById(R.id.ticketsRecycler);
        mSponsorsRV.setAdapter(mAdapter);
        mSponsorsRV.setLayoutManager(new LinearLayoutManager(mContext));

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        loadOrdersList();
    }

    private void loadOrdersList() {
        ApiSingleton.getInstance(mContext)
                .addToRequestQueue(new ApiSingleton.GsonRequest(ApiConfig.ordersList,
                        ApiResponse.class,
                        Request.Method.GET,
                        ApiConfig.getAuthHeaders(), null, mOrderList.isEmpty(),
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {

                                Log.d(TAG, response.toString());
                                ApiResponse res = (ApiResponse) response;
                                mOrderList.clear();
                                mOrderList.addAll(res.data.orders);
                                mAdapter.notifyDataSetChanged();

                                if(mOrderList.isEmpty()) {
                                    v.findViewById(R.id.emptyTicketsView).setVisibility(View.VISIBLE);
                                    mToolbar.findViewById(R.id.toolbar_add).setVisibility(View.GONE);
                                    ((MainActivity)getActivity()).setToolbarTitle(getResources().getString(R.string.tickets_empty_bar_title_label));
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                    }
                }));
    }
}

class OrdersListRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private final List<Order> mItems;
    private OnItemClickListener mListener;

    public OrdersListRecyclerAdapter(Context context, List<Order> orderList, OnItemClickListener listener) {
        mContext = context;
        mItems = orderList;
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_order, parent, false);
        return new OrderHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        Order item = mItems.get(position);
        final OrderHolder orderHolder = (OrderHolder) holder;
        orderHolder.setItem(item);
        orderHolder.mOrderNoTxt.setText("Orden " + formatFolio(item.getId()));
        String status = "Incompleto";
        int statusDrawable = R.drawable.order_draft_back;
        if (item.getPaymentStatus().equalsIgnoreCase("pendiente") && item.getStatus().equalsIgnoreCase("finalizado")) {
            status = "Pendiente";
            statusDrawable = R.drawable.order_pending_back;
        } else if (item.getPaymentStatus().equalsIgnoreCase("pagado") && item.getStatus().equalsIgnoreCase("finalizado")) {
            status = "Pagado";
            statusDrawable = R.drawable.order_finished_back;
        }
        orderHolder.mStatusTxt.setText(status);
        orderHolder.mStatusTxt.setBackground(mContext.getResources().getDrawable(statusDrawable));

        int cashTickets = item.getTickets() - item.getCoupons();
        if( cashTickets > 0 ) {
            orderHolder.mNumTicketsTxt.setText(String.valueOf(cashTickets));
            orderHolder.mTicketsLay.setVisibility(View.VISIBLE);
        } else {
            cashTickets = 0;
            orderHolder.mTicketsLay.setVisibility(View.GONE);
        }

        if( item.getCoupons() > 0 ) {
            orderHolder.mNumCouponsTxt.setText(String.valueOf(item.getCoupons()));
            orderHolder.mCouponsLay.setVisibility(View.VISIBLE);
        } else {
            orderHolder.mCouponsLay.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class OrderHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public final View mView;
        public final TextView mOrderNoTxt;
        public final TextView mStatusTxt;
        public final LinearLayout mTicketsLay;
        public final LinearLayout mCouponsLay;
        public final TextView mNumTicketsTxt;
        public final TextView mNumCouponsTxt;
        private Order mItem;

        public OrderHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            mView = view;
            mOrderNoTxt = (TextView) view.findViewById(R.id.orderNoLabel);
            mStatusTxt = (TextView) view.findViewById(R.id.orderStatusLabel);
            mTicketsLay = (LinearLayout) view.findViewById(R.id.ticketsLay);
            mNumTicketsTxt = (TextView) view.findViewById(R.id.noTicketsLabel);
            mCouponsLay = (LinearLayout) view.findViewById(R.id.couponsLay);
            mNumCouponsTxt = (TextView) view.findViewById(R.id.noCouponsLabel);
        }

        public void setItem(Order item) {
            mItem = item;
        }

        @Override
        public void onClick(View v) {
            mListener.onClick(mItem);
        }
    }

    public interface OnItemClickListener {
        void onClick(Order item);
    }
}


