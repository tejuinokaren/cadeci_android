package com.tejuino.cadeci.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tejuino.cadeci.activities.FoundationActivity;
import com.tejuino.cadeci.activities.MainActivity;
import com.tejuino.cadeci.activities.MyRemindersActivity;
import com.tejuino.cadeci.activities.ProfileEditActivity;
import com.tejuino.cadeci.models.ProfileItem;
import com.tejuino.cadeci.R;
import com.tejuino.cadeci.views.BorderedCircleTransform;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.tejuino.cadeci.activities.MainActivity.CONGRESS_AVAILABLE;
import static com.tejuino.cadeci.models.ProfileItem.TAG_FOUNDATION;
import static com.tejuino.cadeci.models.ProfileItem.TAG_LOGOUT;
import static com.tejuino.cadeci.models.ProfileItem.TAG_REMINDERS;
import static com.tejuino.cadeci.utils.Repository.animCrossfade;
import static com.tejuino.cadeci.utils.SessionManager.sSession;
import static com.tejuino.cadeci.utils.SessionManager.sUser;

public class ProfileFragment extends Fragment {

    public static final int REQUEST_MY_REMINDERS = 101;
    private Context mContext;
    private RecyclerView mProfileRV;
    private List<ProfileItem> mProfileItems = new ArrayList<>();
    private ProfileRecyclerAdapter mProfileAdapter;
    private EditText mNameField, mLastnameField, mPhoneField, mEmailField;
    private TextView mProfileName;
    private ImageView mProfileImg;
    private TextView mProfileEmail;

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mProfileItems.add(new ProfileItem(TAG_FOUNDATION,"Fundación", R.drawable.ic_heart));
        mProfileItems.add(new ProfileItem(TAG_REMINDERS,"Mis recordatorios", R.drawable.ic_logout));
//        mProfileItems.add(new ProfileItem(TAG_CERTIFICATES,"Mis certificados", R.drawable.ic_logout));
        mProfileItems.add(new ProfileItem(TAG_LOGOUT,"Cerrar sesión", R.drawable.ic_logout));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile, container, false);

        mContext = getContext();

        animCrossfade(v.findViewById(R.id.container));

        ((MainActivity)getActivity()).getToolbar().setVisibility(View.GONE);

        mProfileImg = (ImageView) v.findViewById(R.id.profileImg);

        mProfileRV = (RecyclerView) v.findViewById(R.id.profileRecycler);
        mProfileRV.setLayoutManager(new LinearLayoutManager(mContext));
        mProfileAdapter = new ProfileRecyclerAdapter(mProfileItems, mContext, new ProfileRecyclerAdapter.OnItemClickListner() {
            @Override
            public void OnItemClicked(ProfileItem item) {
                switch (item.getId()) {
                    case ProfileItem.TAG_REMINDERS:
                        if(CONGRESS_AVAILABLE) {
                            startActivityForResult(MyRemindersActivity.newIntent(mContext), REQUEST_MY_REMINDERS);
                        } else {
                            new AlertDialog.Builder(mContext)
                                    .setTitle(R.string.coming_soon_alert_title)
                                    .setMessage(R.string.coming_soon_alert_message)
                                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    }).create().show();
                        }
                        break;
                    case ProfileItem.TAG_FOUNDATION:
                        startActivity(FoundationActivity.newIntent(mContext));
                        break;
                    case ProfileItem.TAG_LOGOUT:
                        sSession.logoutUser();
                        break;
                }
            }
        });
        mProfileRV.setAdapter(mProfileAdapter);

        mProfileName = (TextView) v.findViewById(R.id.profileName);
        mProfileEmail = (TextView) v.findViewById(R.id.profileEmail);

        v.findViewById(R.id.editBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(ProfileEditActivity.newIntent(mContext));
            }
        });

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        mProfileName.setText(sUser.getInstance().getName() + " " + sUser.getLastName());
        mProfileEmail.setText(sUser.getInstance().getEmail());
        Picasso.with(mContext)
                .load(sUser.getInstance().getImage())
                .resize(400, 400)
                .centerInside()
                .placeholder(R.drawable.shape_circle_gray)
                .transform(new BorderedCircleTransform(10, getResources().getColor(R.color.colorAccent)))
                .into(mProfileImg);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == ProfileFragment.REQUEST_MY_REMINDERS) {
            if(resultCode == RESULT_OK) {
                ((MainActivity) getActivity()).performClickBottomNavItem(R.id.navigation_congress);
            }
        }
    }
}


class ProfileRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int ITEM_PROFILE = 1;

    private List<ProfileItem> mProfileItems = new ArrayList<>();
    private Context mContext;
    OnItemClickListner mListner;

    public ProfileRecyclerAdapter(List<ProfileItem> profileItems, Context context, OnItemClickListner listener) {
        mProfileItems = profileItems;
        mContext = context;
        mListner = listener;
    }

    public class ProfileItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProfileItem mItem;
        private TextView mTitle;
        //private ImageView mImage;

        public ProfileItemHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            //mImage = (ImageView) v.findViewById(R.id.profileItemImg);
            mTitle = (TextView) v.findViewById(R.id.profileItemTxt);
        }

        public void setItem(ProfileItem item) {
            mItem = item;
        }

        @Override
        public void onClick(View v) {
            mListner.OnItemClicked(mItem);
        }
    }


    @Override
    public int getItemViewType(int position) {
        //return mUsersList.get(position).sUser != null ? ITEM_USER : ITEM_PROFILE;
        return ITEM_PROFILE;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        switch (viewType) {
            case ITEM_PROFILE:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_profile, parent, false);
                return new ProfileItemHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final int itemType = getItemViewType(position);
        switch (itemType) {
            case ITEM_PROFILE:
                ProfileItemHolder itemHolder = (ProfileItemHolder) holder;
                ProfileItem profileItem = mProfileItems.get(position);
                itemHolder.setItem(profileItem);

                itemHolder.mTitle.setText(profileItem.getTitle());
                //itemHolder.mImage.setImageResource(profileItem.getImage());

                break;
        }
    }

    @Override
    public int getItemCount() {
        return mProfileItems.size();
    }

    public interface OnItemClickListner {
        void OnItemClicked(ProfileItem item);
    }
}
