package com.tejuino.cadeci.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;
import com.tejuino.cadeci.models.ApiResponse;
import com.tejuino.cadeci.models.Schedule;
import com.tejuino.cadeci.models.Speaker;
import com.tejuino.cadeci.network.ApiConfig;
import com.tejuino.cadeci.network.ApiSingleton;
import com.tejuino.cadeci.R;
import com.tejuino.cadeci.views.SquareLinearLayoutWidth;

import org.w3c.dom.Text;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;
import static com.tejuino.cadeci.network.ApiConfig.saveReminder;

/**
 * Created by dianakarenms@gmail.com on 04/08/17.
 */

public class ScheduleChildTabFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SCHEDULE = "schedule_data";
    private ScheduleRecyclerAdapter adapter;
    private ScheduleRecyclerAdapter.OnItemClickListener mListener;
    private List<Schedule> mScheduleList = new ArrayList<>();
    private RecyclerView mScheduleRV;

    public ScheduleChildTabFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static ScheduleChildTabFragment newInstance(List<Schedule> schedule, int fragNum) {
        ScheduleChildTabFragment fragment = new ScheduleChildTabFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_SCHEDULE, (Serializable) schedule);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_schedule_child_tabbed, container, false);

        mScheduleList = (List<Schedule>) getArguments().getSerializable(ARG_SCHEDULE);

        TextView weekDay = (TextView) rootView.findViewById(R.id.week_day_label);
        TextView day = (TextView) rootView.findViewById(R.id.day_label);
        if(mScheduleList.get(0).getStart() != null) {
            weekDay.setText(mScheduleList.get(0).getScheduleWeekDayString());
            day.setText(mScheduleList.get(0).getScheduleDayString());
        } else {
            int backColor = getResources().getColor(R.color.commentsBack);
            weekDay.setBackgroundColor(backColor);
            day.setBackgroundColor(backColor);
        }

        mScheduleRV = (RecyclerView) rootView.findViewById(R.id.schedule_recycler);
        mScheduleRV.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new ScheduleRecyclerAdapter(getContext(), mScheduleList, new ScheduleRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(final Schedule schedule, int position) {
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(getContext(), android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
                } else {
                    builder = new AlertDialog.Builder(getContext());
                }
                builder.setTitle("Agregar Recordatorio")
                        .setMessage("Te notificaremos 1 hora antes de que inicie esta conferencia: \"" + schedule.getTitle() + "\"")
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
//                                storeEventInCalendarIntent(schedule, getActivity());
                                storeEventInServer(schedule, getActivity());

                                /*Toast.makeText(getContext(),
                                        "Store reminder in server and receive trigger by push",
                                        Toast.LENGTH_LONG)
                                        .show();*/
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });
        mScheduleRV.setAdapter(adapter);
        return rootView;
    }

    private void storeEventInServer(Schedule schedule, final Context context) {
    // Access the RequestQueue through your singleton class.
        String url = String.format(saveReminder, schedule.getId());
        ApiSingleton.getInstance(context)
                .addToRequestQueue(new ApiSingleton.GsonRequest(url,
                        ApiResponse.class,
                        Request.Method.POST,
                        ApiConfig.getAuthHeaders(), null, false,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());

                                Toast.makeText(context, R.string.save_success, Toast.LENGTH_SHORT).show();

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
//                        mPaginationRefreshRecycler.stopRefresh();
                    }
                }));
    }
}

class ScheduleRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int TYPE_DUMMY = -1;
    public static final int TYPE_MAIN = 0;
    public static final int TYPE_SECONDARY = 1;

    private final List<Schedule> mValues;
    private final Context mContext;
    private OnItemClickListener mListener;

    public ScheduleRecyclerAdapter(Context context, List<Schedule> items, OnItemClickListener listener) {
        mContext = context;
        mValues = items;
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case TYPE_MAIN:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_schedule_main, parent, false);
                return new MainSchHolder(view);
            case TYPE_SECONDARY:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_schedule_secondary, parent, false);
                return new SecondarySchHolder(view);
            default:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_schedule_secondary, parent, false);
                return new SecondarySchHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, int position) {
        Schedule item = mValues.get(position);
        if(item != null) {
            switch (getItemViewType(position)) {
                case TYPE_MAIN:
                    final MainSchHolder mainSchHolder = (MainSchHolder) viewHolder;
                    mainSchHolder.setItem(item);
                    mainSchHolder.mTitleTxt.setText(item.getTitle());
                    mainSchHolder.mNameTxt.setText(getSpeakersByComma(item.getSpeakers()));
                    mainSchHolder.mTimeTxt.setText(item.getStartHourString() + " - " + item.getFinishHourString());
                    if(!item.getSponsor().isEmpty()) {
                        mainSchHolder.mSponsorLay.setVisibility(View.VISIBLE);
                        Picasso.with(mContext)
                                .load(item.getSponsor())
                                .resize(200, 200)
                                .centerInside()
                                .into(mainSchHolder.mSponsorImg);
                    }
                    break;
                case TYPE_SECONDARY:
                    final SecondarySchHolder secondarySchHolder = (SecondarySchHolder) viewHolder;
                    secondarySchHolder.setItem(item);
                    secondarySchHolder.mTitleTxt.setText(item.getTitle());
                    secondarySchHolder.mNameTxt.setText(getSpeakersByComma(item.getSpeakers()));
                    secondarySchHolder.mStartTimeTxt.setText(item.getStartHourString());
                    secondarySchHolder.mEndTimeTxt.setText(item.getFinishHourString());
                    secondarySchHolder.mSubtitleTxt.setText(item.getDescription());
                    secondarySchHolder.mSpotTxt.setText("Salón: " + item.getSpot().getTitle());
                    break;
                default:
                    final SecondarySchHolder dummySchHolder = (SecondarySchHolder) viewHolder;
                    int backColor = mContext.getResources().getColor(R.color.dummiesBack);
                    dummySchHolder.setItem(item);
                    dummySchHolder.mTitleTxt.setBackgroundColor(backColor);
                    dummySchHolder.mNameTxt.setBackgroundColor(backColor);
                    dummySchHolder.mStartTimeTxt.setBackgroundColor(backColor);
                    dummySchHolder.mEndTimeTxt.setBackgroundColor(backColor);
                    dummySchHolder.mSubtitleTxt.setBackgroundColor(backColor);
                    dummySchHolder.mSpotTxt.setBackgroundColor(backColor);
                    dummySchHolder.mHourCircle.setBackgroundColor(mContext.getResources().getColor(R.color.backgroundPrimaryLight));
                    dummySchHolder.setClickable(false);
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class MainSchHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final View mView;
        public final TextView mTitleTxt;
        public final TextView mNameTxt;
        public final TextView mTimeTxt;
        public final LinearLayout mSponsorLay;
        public final ImageView mSponsorImg;
        private Schedule mItem;

        public MainSchHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            mView = view;
            mTitleTxt = (TextView) view.findViewById(R.id.title_txt);
            mNameTxt = (TextView) view.findViewById(R.id.name_txt);
            mTimeTxt = (TextView) view.findViewById(R.id.time_txt);
            mSponsorLay = (LinearLayout) view.findViewById(R.id.logo);
            mSponsorImg = (ImageView) view.findViewById(R.id.sponsorImg);
        }

        public void setItem(Schedule item) {
            mItem = item;
        }

        @Override
        public void onClick(View v) {
            mListener.onItemClick(mItem, getAdapterPosition());
        }
    }

    public class SecondarySchHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final View mView;
        public final TextView mTitleTxt;
        public final TextView mNameTxt;
        public final TextView mStartTimeTxt;
        public final TextView mEndTimeTxt;
        public final TextView mSubtitleTxt;
        public final TextView mSpotTxt;
        public final SquareLinearLayoutWidth mHourCircle;
        private Schedule mItem;
        private boolean isClickable = true;

        public SecondarySchHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            mView = view;
            mTitleTxt = (TextView) view.findViewById(R.id.title_txt);
            mNameTxt = (TextView) view.findViewById(R.id.name_txt);
            mStartTimeTxt = (TextView) view.findViewById(R.id.start_time_txt);
            mEndTimeTxt = (TextView) view.findViewById(R.id.end_time_txt);
            mSubtitleTxt = (TextView) view.findViewById(R.id.subtitle_txt);
            mSpotTxt = (TextView) view.findViewById(R.id.spotTxt);
            mHourCircle = (SquareLinearLayoutWidth) view.findViewById(R.id.hourCircle);
        }

        public void setItem(Schedule item) {
            mItem = item;
        }

        public void setClickable(boolean isClickable) {
            this.isClickable = isClickable;
        }

        @Override
        public void onClick(View v) {
            if (isClickable) {
                mListener.onItemClick(mItem, getAdapterPosition());
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        switch (mValues.get(position).getType()) {
            case Schedule.TAG_MAIN:
                return 0;
            case Schedule.TAG_SECONDARY:
                return 1;
            default:
                return -1;
        }
    }

    public interface OnItemClickListener {
        public void onItemClick(Schedule schedule, int position);
    }

    private String getSpeakersByComma(List<Speaker> list) {
        String answer = "";
        Speaker speaker = null;
        for(int i = 0; i < list.size(); i++){
            speaker = list.get(i);
            answer = answer + speaker.getName() + " (" + speaker.getHometown() + ")";
            if(i != list.size()-1) {
                answer = answer + ",\n";
            }
        }
        return answer;
    }
}