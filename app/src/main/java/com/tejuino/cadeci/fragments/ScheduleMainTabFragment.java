package com.tejuino.cadeci.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.CalendarContract;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.tejuino.cadeci.activities.CongressMapActivity;
import com.tejuino.cadeci.activities.MainActivity;
import com.tejuino.cadeci.models.ApiResponse;
import com.tejuino.cadeci.models.Comment;
import com.tejuino.cadeci.models.Schedule;
import com.tejuino.cadeci.R;
import com.tejuino.cadeci.network.ApiConfig;
import com.tejuino.cadeci.network.ApiSingleton;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import static com.tejuino.cadeci.utils.Repository.animCrossfade;

public class ScheduleMainTabFragment extends Fragment {

    private static final String ARG_SCHEDULES_LIST = "schedule";
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SchedulePagerAdapter mSchedulePagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private Context mContext;
    private Comment comment;
    private Schedule mEvent;
    private LinkedHashMap<String, List<Schedule>> mSchedulesList = new LinkedHashMap<>();

    public static ScheduleMainTabFragment newInstance() {
        ScheduleMainTabFragment f = new ScheduleMainTabFragment();
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        // Empty scheduleTab
        if(mSchedulesList.isEmpty()) {
            List<Schedule> dummySch = new ArrayList<>();
            for (int i = 1; i <= 3; i++)
                dummySch.add(new Schedule());
            mSchedulesList.put("", dummySch);
        }

        mSchedulePagerAdapter = new SchedulePagerAdapter(getChildFragmentManager(), mSchedulesList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_schedule_tabbed, container, false);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mContext = getContext();

        animCrossfade(v.findViewById(R.id.container));

        // Set toolbar title
        ((MainActivity)getActivity()).setToolbarTitle(getResources().getString(R.string.schedule_bar_title_label));
        Toolbar toolbar = ((MainActivity)getActivity()).getToolbar();
        toolbar.setVisibility(View.VISIBLE);
        ImageButton mapBtn = (ImageButton) toolbar.findViewById(R.id.toolbar_map);
        mapBtn.setVisibility(View.VISIBLE);
        mapBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CongressMapActivity.loadCongressMap(mContext);
            }
        });


        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) v.findViewById(R.id.scheduleViewPager);
        mViewPager.setAdapter(mSchedulePagerAdapter);
        mViewPager.setOffscreenPageLimit(3);

        // Give the TabLayout the ViewPager
        TabLayout tabLayout = (TabLayout) v.findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(mViewPager);

        if(mSchedulesList.get("") != null)
            loadSchedule();

        return v;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mSchedulePagerAdapter != null) {
            mSchedulePagerAdapter.clearReference();
            mSchedulePagerAdapter = null;
        }
    }

    private void loadSchedule() {
        // Access the RequestQueue through your singleton class.
        ApiSingleton.getInstance(mContext)
                .addToRequestQueue(new ApiSingleton.GsonRequest(ApiConfig.schedule,
                        ApiResponse.class,
                        Request.Method.GET,
                        ApiConfig.getUnauthenticatedHeaders(), null, false,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());
                                ApiResponse res = (ApiResponse) response;
                                mSchedulesList.clear();
                                mSchedulesList.putAll(res.data.schedules);
                                mSchedulePagerAdapter.notifyDataSetChanged();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                    }
                }));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private static final String TAG = "Ext_Storage_Permission";
    private static final int PERMISSION_REQUEST_CODE = 1;

    public static void storeEventInCalendar(Schedule schedule, Activity activity) {
        if (isCalendarPermissionGranted(activity)) {
            Cursor cursor = null;

            String[] projection = new String[]{
                    CalendarContract.Calendars._ID,
                    CalendarContract.Calendars.ACCOUNT_NAME,};

            try {
                ContentResolver cr = activity.getContentResolver();
                cursor = cr.query(Uri.parse("content://com.android.calendar/calendars"), projection, null, null, null);

                int[] calIds = {};
                if (cursor.moveToFirst()) {
                    final String[] calNames = new String[cursor.getCount()];
                    calIds = new int[cursor.getCount()];
                    for (int i = 0; i < calNames.length; i++) {
                        calIds[i] = cursor.getInt(0);
                        calNames[i] = cursor.getString(1);
                        cursor.moveToNext();
                    }
                }

                // Get FechaInicio from cal
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
                Date convertedDate = new Date();
                try {
                    convertedDate = dateFormat.parse(schedule.getStart());
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                cal.setTime(convertedDate);
                ContentValues values = new ContentValues();
                values.put(CalendarContract.Events.DTSTART, cal.getTimeInMillis());

                // Get FechaFin from cal
                try {
                    convertedDate = dateFormat.parse(schedule.getFinish());
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                cal.setTime(convertedDate);
                values.put(CalendarContract.Events.DTEND, cal.getTimeInMillis());
                values.put(CalendarContract.Events.TITLE, schedule.getTitle());
                values.put(CalendarContract.Events.DESCRIPTION, schedule.getDescription());
                values.put(CalendarContract.Events.CALENDAR_ID, calIds[0]);
                values.put(CalendarContract.Events.EVENT_LOCATION, "");
                values.put(CalendarContract.Events.EVENT_TIMEZONE, "GMT-06:00"); // México City


                if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the sUser grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                cr.insert(CalendarContract.Events.CONTENT_URI, values);

                Toast.makeText(activity, "Evento agregado al calendario",
                        Toast.LENGTH_LONG).show();

            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(activity, "Exception: " + e.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }
    public static Boolean isCalendarPermissionGranted(Activity activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (Build.VERSION.SDK_INT >= 23)
            {
                if (checkPermission(activity))
                {
                    return true;
                } else {
                    requestPermission(activity); // Code for permission
                    return false;
                }
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
        return false;
    }

    private static boolean checkPermission(Activity activity) {

        int result = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_CALENDAR);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private static void requestPermission(Activity activity) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, android.Manifest.permission.WRITE_CALENDAR)) {
            Toast.makeText(activity, "Habilita el permiso para guardar el evento en tu calendario.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(activity, new String[]{android.Manifest.permission.WRITE_CALENDAR}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    storeEventInCalendar(mEvent, getActivity());
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Toast.makeText(mContext, "Habilita el permiso para guardar el evento en tu calendario.", Toast.LENGTH_LONG).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}

class SchedulePagerAdapter extends FragmentStatePagerAdapter {

    private LinkedHashMap<String, List<Schedule>> mSchedule = new LinkedHashMap<>();

    // Prevents losing fragment state
    @Override
    public Parcelable saveState() {
        return null;
    }

    public SchedulePagerAdapter(FragmentManager fm, LinkedHashMap<String, List<Schedule>> schedule) {
        super(fm);
        mSchedule = schedule;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        ArrayList<String> keyArr = new ArrayList<>(mSchedule.keySet());
        String title = keyArr.get(position);
        List<Schedule> item = mSchedule.get(title);
        return ScheduleChildTabFragment.newInstance(item, position);
    }

    @Override
    public int getCount() {
        // Show total pages.
        return mSchedule.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(!mSchedule.isEmpty()) {
            String title = "";
            ArrayList<String> keyArr = new ArrayList<>(mSchedule.keySet());
            String date = keyArr.get(position);

            // For dummy content
            if(date.isEmpty()) {
                return "                          ";
            }

            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
            Date dt1= null;
            try {
                dt1 = format1.parse(date);
                DateFormat format2=new SimpleDateFormat("EE", new Locale("es", "ES"));
                title = format2.format(dt1);
            } catch (ParseException e) {
                e.printStackTrace();
                title = "";
            }
            return title;
        } else {
            return null;
        }
    }

    // On calling notifyDataSetChanged(), the view pager will
    // remove all views and reload them all.
    // As so the reload effect is obtained.
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void restoreState(Parcelable arg0, ClassLoader arg1) {
        //do nothing here! no call to super.restoreState(arg0, arg1);
    }

    public void clearReference() {
        if(mSchedule != null) {
            mSchedule.clear();
        }
    }

}
