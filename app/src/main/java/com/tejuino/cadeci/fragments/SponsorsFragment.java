package com.tejuino.cadeci.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView;
import com.squareup.picasso.Picasso;
import com.tejuino.cadeci.activities.MainActivity;
import com.tejuino.cadeci.models.ApiResponse;
import com.tejuino.cadeci.models.Sponsor;
import com.tejuino.cadeci.R;
import com.tejuino.cadeci.network.ApiConfig;
import com.tejuino.cadeci.network.ApiSingleton;
import com.tejuino.cadeci.utils.Repository;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;
import static com.tejuino.cadeci.utils.Repository.animCrossfade;

/**
 * Created by karen on 10/08/17.
 */

public class SponsorsFragment extends Fragment {
    private FastScrollRecyclerView mSponsorsRV;
    private Context mContext;
    private SponsorsRecyclerAdapter mSponsorsAdapter;
    private List<Sponsor> mSponsorsList = new ArrayList<>();

    public static SponsorsFragment newInstance() {
        SponsorsFragment fragment = new SponsorsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();

        if(mSponsorsList.isEmpty()) {
            for (int i = 1; i <= 4; i++) {
                mSponsorsList.add(new Sponsor().setType(Sponsor.TAG_DUMMY));
            }
        }

        mSponsorsAdapter = new SponsorsRecyclerAdapter(mContext, mSponsorsList);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_sponsors_list, container, false);

        animCrossfade(v.findViewById(R.id.container));

        // Set toolbar title
        ((MainActivity)getActivity()).setToolbarTitle(getResources().getString(R.string.sponsors_bar_title));
        ((MainActivity)getActivity()).getToolbar().setVisibility(View.VISIBLE);

        mSponsorsRV = (FastScrollRecyclerView) v.findViewById(R.id.list);
        mSponsorsRV.setLayoutManager(new LinearLayoutManager(mContext));
        mSponsorsRV.setAdapter(mSponsorsAdapter);

        if(mSponsorsList.get(0).getType().equals("dummy"))
            loadSponsors();

        TextView searchTxt = (TextView) v.findViewById(R.id.search_edit);
        searchTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mSponsorsAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return v;
    }

    private void loadSponsors() {
        // Access the RequestQueue through your singleton class.
        ApiSingleton.getInstance(mContext)
                .addToRequestQueue(new ApiSingleton.GsonRequest(ApiConfig.sponsors,
                        ApiResponse.class,
                        Request.Method.GET,
                        ApiConfig.getUnauthenticatedHeaders(), null, false,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());
                                ApiResponse res = (ApiResponse) response;
                                mSponsorsList.clear();
                                mSponsorsList.addAll(res.data.sponsors);
                                mSponsorsAdapter.notifyDataSetChanged();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                    }
                }));

    }
}

class SponsorsRecyclerAdapter extends RecyclerView.Adapter<SponsorsRecyclerAdapter.ViewHolder>
        implements FastScrollRecyclerView.SectionedAdapter,
        FastScrollRecyclerView.MeasurableAdapter, Filterable {

    public static final int TYPE_DUMMY = -1;
    public static final int TYPE_MAIN = 0;

    private final Context mContext;
    private List<Sponsor> mSponsorList;
    private List<Sponsor> mOrigSponsorList;
    private SponsorsFilter contactFilter;

    public SponsorsRecyclerAdapter(Context context, List<Sponsor> items) {
        mOrigSponsorList = items;
        mSponsorList = mOrigSponsorList;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sponsor, parent, false));
    }

    @Override
    public int getItemViewType(int position) {
        switch (mSponsorList.get(position).getType()) {
            case Sponsor.TAG_MAIN:
                return TYPE_MAIN;
            default:
                return TYPE_DUMMY;
        }
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Sponsor item = mSponsorList.get(position);
        if(item != null) {
            switch (getItemViewType(position)) {
                case TYPE_MAIN:
                    final ViewHolder secondarySchHolder = (ViewHolder) holder;
                    secondarySchHolder.setItem(item);
                    Picasso.with(mContext)
                            .load(item.getImage())
                            .resize(300, 300)
                            .centerInside()
                            .into(secondarySchHolder.mSponsorImg);
                    break;
                default:
                    final ViewHolder dummySchHolder = (ViewHolder) holder;
                    int backColor = mContext.getResources().getColor(R.color.backgroundPrimaryLight);
                    dummySchHolder.setItem(item);
                    dummySchHolder.mSponsorImg.setBackgroundColor(backColor);
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return mSponsorList.size();
    }

    @NonNull
    @Override
    public String getSectionName(int position) {
        return mSponsorList.get(position).getTitle().substring(0, 1);
    }

    @Override
    public int getViewTypeHeight(RecyclerView recyclerView, int viewType) {
        if (viewType == TYPE_MAIN) {
            return recyclerView.getResources().getDimensionPixelSize(R.dimen.sponsor_item_height);
        }
        return 0;
    }

    @Override
    public Filter getFilter() {
        if (contactFilter == null)
            contactFilter = new SponsorsFilter();

        return contactFilter;
    }

    private class SponsorsFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults result = new FilterResults();
            if(constraint == null || constraint.length() == 0) {
                result.values = mOrigSponsorList;
                result.count = mOrigSponsorList.size();
            } else {
                ArrayList<Sponsor> filteredList = new ArrayList<>();
                for(Sponsor j: mOrigSponsorList) {
                    if (j.getTitle().toUpperCase().startsWith(constraint.toString().toUpperCase()))
                        filteredList.add(j);
                }
                result.values = filteredList;
                result.count = filteredList.size();
            }

            return result;
        }
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mSponsorList = (ArrayList<Sponsor>) results.values; // addAll doesn't work in here
            notifyDataSetChanged();

        }

    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final View mView;
        public final ImageView mSponsorImg;
        private Sponsor mItem;

        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            mView = view;
            mSponsorImg = (ImageView) view.findViewById(R.id.sponsorImg);
        }

        public void setItem(Sponsor item) {
            mItem = item;
        }

        @Override
        public void onClick(View v) {
            String url = mItem.getUrl();
            Repository.openPdfURL(url, mContext);
        }
    }
}
