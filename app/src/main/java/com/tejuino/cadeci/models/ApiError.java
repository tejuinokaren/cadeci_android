package com.tejuino.cadeci.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by karen on 06/09/17.
 */

public class ApiError implements Serializable {
    @SerializedName("internal")
    @Expose
    public String internal;
    @SerializedName("external")
    @Expose
    public String external;
}
