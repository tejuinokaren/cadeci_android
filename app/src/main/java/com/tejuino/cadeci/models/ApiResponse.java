package com.tejuino.cadeci.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by karen on 27/07/17.
 */

public class ApiResponse implements Serializable {
    @SerializedName("data")
    @Expose
    public Data data;
    @SerializedName("error")
    @Expose
    public ApiError error;
}
