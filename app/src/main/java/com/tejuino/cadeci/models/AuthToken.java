package com.tejuino.cadeci.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by karen on 11/04/17.
 */

public class AuthToken implements Serializable {
    private static volatile AuthToken instance = new AuthToken();

    public static AuthToken getInstance() {
        if (instance == null) {
            synchronized (User.class) {
                if (instance == null) {
                    instance = new AuthToken();
                }
            }
        }
        return instance;
    }

    public static void setInstance(AuthToken instance) {
        AuthToken.instance = instance;
    }

    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("token_type")
    @Expose
    private String tokenType;
    @SerializedName("expires_in")
    @Expose
    private Integer expiresIn;
    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @SerializedName("refresh_token")
    @Expose
    private String refreshToken;
    private Date expiresAtDate;

    /** Setters **/

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public Integer getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Integer expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public Date getExpiresAtDate() {
        return expiresAtDate;
    }

    public void setExpiresAtDate(Date expiresAtDate) {
        this.expiresAtDate = expiresAtDate;
    }
}
