package com.tejuino.cadeci.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by karen on 19/09/17.
 */

public class ConektaToken implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("livemode")
    @Expose
    private Boolean livemode;
    @SerializedName("used")
    @Expose
    private Boolean used;
    @SerializedName("object")
    @Expose
    private String object;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getLivemode() {
        return livemode;
    }

    public void setLivemode(Boolean livemode) {
        this.livemode = livemode;
    }

    public Boolean getUsed() {
        return used;
    }

    public void setUsed(Boolean used) {
        this.used = used;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

}
