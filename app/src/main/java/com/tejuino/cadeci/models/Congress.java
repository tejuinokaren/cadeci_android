package com.tejuino.cadeci.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by karen on 16/10/17.
 */

public class Congress implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("image_map")
    @Expose
    private String imageMap;
    @SerializedName("privacy_file")
    @Expose
    private String privacyFile;
    @SerializedName("conditions_file")
    @Expose
    private String conditionsFile;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("inscriptions_ends_at")
    @Expose
    private String inscriptionsEndsAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageMap() {
        return imageMap;
    }

    public void setImageMap(String imageMap) {
        this.imageMap = imageMap;
    }

    public String getPrivacyFile() {
        return privacyFile;
    }

    public void setPrivacyFile(String privacyFile) {
        this.privacyFile = privacyFile;
    }

    public String getConditionsFile() {
        return conditionsFile;
    }

    public void setConditionsFile(String conditionsFile) {
        this.conditionsFile = conditionsFile;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInscriptionsEndsAt() {
        return inscriptionsEndsAt;
    }

    public void setInscriptionsEndsAt(String inscriptionsEndsAt) {
        this.inscriptionsEndsAt = inscriptionsEndsAt;
    }
}
