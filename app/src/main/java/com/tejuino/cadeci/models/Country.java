package com.tejuino.cadeci.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by karen on 09/10/17.
 */

public class Country extends Place implements Serializable {
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("phone_code")
    @Expose
    private String phoneCode;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public boolean isMexico() {
        if(this.getId() == 135) {
            return true;
        }
        return false;
    }
}
