package com.tejuino.cadeci.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by karen on 31/07/17.
 */

public class Data implements Serializable {
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("user")
    @Expose
    public User user;
    @SerializedName("posts")
    @Expose
    public PostsPage mPostsPage;
    @SerializedName("post")
    @Expose
    public Post mPost;
    @SerializedName("comments")
    @Expose
    public List<Comment> comments = null;
    @SerializedName("schedules")
    @Expose
    public LinkedHashMap<String, List<Schedule>> schedules;
    @SerializedName("sponsors")
    @Expose
    public List<Sponsor> sponsors = null;
    @SerializedName("reminders")
    @Expose
    public List<Reminder> reminders = null;
    @SerializedName("professions")
    @Expose
    public List<Profession> professions = null;
    @SerializedName("order")
    @Expose
    public Order order;
    @SerializedName("orders")
    @Expose
    public List<Order> orders = null;
    @SerializedName("countries")
    @Expose
    public List<Country> countries = null;
    @SerializedName("states")
    @Expose
    public List<State> states = null;
    @SerializedName("cities")
    @Expose
    public List<City> cities = null;
    @SerializedName("congress")
    @Expose
    public Congress congress;
    @SerializedName("device_id")
    @Expose
    public int deviceId;
}
