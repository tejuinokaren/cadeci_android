package com.tejuino.cadeci.models;

import java.io.Serializable;

/**
 * Created by karen on 21/09/17.
 */

public class Header implements Serializable {
    private String title;

    public Header() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
