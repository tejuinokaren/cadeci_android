package com.tejuino.cadeci.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by karen on 25/09/17.
 */

public class Order implements Serializable {
//    public static final int TAG_MAIN = 0;
//    public static final int TAG_DUMMY = 1;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("congress_id")
    @Expose
    private Integer congressId;
    @SerializedName("subtotal")
    @Expose
    private String subtotal;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    @SerializedName("payment_status")
    @Expose
    private String paymentStatus;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("invoice_required")
    @Expose
    private Integer invoiceRequired;
    @SerializedName("invoice_rfc")
    @Expose
    private String invoiceRfc;
    @SerializedName("invoice_name")
    @Expose
    private String invoiceName;
    @SerializedName("invoice_street")
    @Expose
    private String invoiceStreet;
    @SerializedName("invoice_external_number")
    @Expose
    private String invoiceExternalNumber;
    @SerializedName("invoice_internal_number")
    @Expose
    private String invoiceInternalNumber;
    @SerializedName("invoice_suburb")
    @Expose
    private String invoiceSuburb;
    @SerializedName("invoice_city")
    @Expose
    private String invoiceCity;
    @SerializedName("invoice_state")
    @Expose
    private String invoiceState;
    @SerializedName("invoice_zip")
    @Expose
    private String invoiceZip;
    @SerializedName("tickets")
    @Expose
    private Integer tickets;
    @SerializedName("coupons")
    @Expose
    private Integer coupons;
    @SerializedName("payment_file")
    @Expose
    private String paymentFile;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("inscriptions")
    @Expose
    private List<Inscription> inscriptions = new ArrayList<>();
//    private int type;

   /* public Order() {
        this.type = TAG_DUMMY;
    }*/

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCongressId() {
        return congressId;
    }

    public void setCongressId(Integer congressId) {
        this.congressId = congressId;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Inscription> getInscriptions() {
        return inscriptions;
    }

    public void setInscriptions(List<Inscription> inscriptions) {
        this.inscriptions = inscriptions;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public Integer getInvoiceRequired() {
        return invoiceRequired;
    }

    public void setInvoiceRequired(Integer invoiceRequired) {
        this.invoiceRequired = invoiceRequired;
    }

    public String getInvoiceRfc() {
        return invoiceRfc;
    }

    public void setInvoiceRfc(String invoiceRfc) {
        this.invoiceRfc = invoiceRfc;
    }

    public String getInvoiceName() {
        return invoiceName;
    }

    public void setInvoiceName(String invoiceName) {
        this.invoiceName = invoiceName;
    }

    public String getInvoiceStreet() {
        return invoiceStreet;
    }

    public void setInvoiceStreet(String invoiceStreet) {
        this.invoiceStreet = invoiceStreet;
    }

    public String getInvoiceExternalNumber() {
        return invoiceExternalNumber;
    }

    public void setInvoiceExternalNumber(String invoiceExternalNumber) {
        this.invoiceExternalNumber = invoiceExternalNumber;
    }

    public String getInvoiceInternalNumber() {
        return invoiceInternalNumber;
    }

    public void setInvoiceInternalNumber(String invoiceInternalNumber) {
        this.invoiceInternalNumber = invoiceInternalNumber;
    }

    public String getInvoiceSuburb() {
        return invoiceSuburb;
    }

    public void setInvoiceSuburb(String invoiceSuburb) {
        this.invoiceSuburb = invoiceSuburb;
    }

    public String getInvoiceCity() {
        return invoiceCity;
    }

    public void setInvoiceCity(String invoiceCity) {
        this.invoiceCity = invoiceCity;
    }

    public String getInvoiceState() {
        return invoiceState;
    }

    public void setInvoiceState(String invoiceState) {
        this.invoiceState = invoiceState;
    }

    public String getInvoiceZip() {
        return invoiceZip;
    }

    public void setInvoiceZip(String invoiceZip) {
        this.invoiceZip = invoiceZip;
    }

    public Integer getTickets() {
        return tickets;
    }

    public void setTickets(Integer tickets) {
        this.tickets = tickets;
    }

    public Integer getCoupons() {
        return coupons;
    }

    public void setCoupons(Integer coupons) {
        this.coupons = coupons;
    }

    public String getPaymentFile() {
        return paymentFile;
    }

    public void setPaymentFile(String paymentFile) {
        this.paymentFile = paymentFile;
    }
}
