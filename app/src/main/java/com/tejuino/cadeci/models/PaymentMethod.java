package com.tejuino.cadeci.models;

/**
 * Created by karen on 29/08/17.
 */

public class PaymentMethod {
    public int id;
    public String name;
    public int drawableImg;

    public PaymentMethod(int id, String name, int drawableImg) {
        this.id = id;
        this.name = name;
        this.drawableImg = drawableImg;
    }
}