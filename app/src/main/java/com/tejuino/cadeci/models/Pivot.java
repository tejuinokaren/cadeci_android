package com.tejuino.cadeci.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by karen on 03/08/17.
 */

public class Pivot implements Serializable {
    @SerializedName("schedule_id")
    @Expose
    private Integer scheduleId;
    @SerializedName("speaker_id")
    @Expose
    private Integer speakerId;

    public Integer getScheduleId() {
        return scheduleId;
    }

    public Integer getSpeakerId() {
        return speakerId;
    }

    public void setSpeakerId(Integer speakerId) {
        this.speakerId = speakerId;
    }
}
