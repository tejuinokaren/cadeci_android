package com.tejuino.cadeci.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by karen on 31/07/17.
 */

public class Post implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("extract")
    @Expose
    private String extract;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("outstanding")
    @Expose
    private Integer outstanding;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("published")
    @Expose
    private Integer published;
    @SerializedName("author")
    @Expose
    private Author author;
    @SerializedName("category")
    @Expose
    private PostCategory category;
    @SerializedName("posts_related")
    @Expose
    private List<PostsRelated> mPostsRelatedList = null;
    @SerializedName("messages")
    @Expose
    private List<PostMessage> messages = null;

    public Integer getId() {
        return id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getExtract() {
        return extract;
    }

    public void setExtract(String extract) {
        this.extract = extract;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getOutstanding() {
        return outstanding;
    }

    public void setOutstanding(Integer outstanding) {
        this.outstanding = outstanding;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getPublished() {
        return published;
    }

    public void setPublished(Integer published) {
        this.published = published;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public PostCategory getCategory() {
        return category;
    }

    public void setCategory(PostCategory category) {
        this.category = category;
    }

    public List<PostsRelated> getPostsRelatedList() {
        return mPostsRelatedList;
    }

    public void setPostsRelatedList(List<PostsRelated> postsRelatedList) {
        mPostsRelatedList = postsRelatedList;
    }

    public List<PostMessage> getMessages() {
        return messages;
    }

    public void setMessages(List<PostMessage> messages) {
        this.messages = messages;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCreatedAtString() {
        String dateString = "";
        String date = createdAt;
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dt1 = null;
        try {
            dt1 = format1.parse(date);
            DateFormat format2 = new SimpleDateFormat("dd MMMM yyyy", new Locale("es", "ES"));
            dateString = format2.format(dt1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateString;
    }
}
