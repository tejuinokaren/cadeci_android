package com.tejuino.cadeci.models;

/**
 * Created by karen on 31/08/17.
 */

public class ProfileItem {

    public static final int TAG_LOGOUT = 0;
    public static final int TAG_FOUNDATION = 1;
    public static final int TAG_CERTIFICATES = 2;
    public static final int TAG_REMINDERS = 3;

    private int id;
    private String title;
    private int image;

    public ProfileItem(int id, String title, int image) {
        this.id = id;
        this.title = title;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
