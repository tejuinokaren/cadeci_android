package com.tejuino.cadeci.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by karen on 18/09/17.
 */

public class Reminder extends Object implements Serializable, Cloneable {
    public static final String TAG_HEADER = "header";
    public static final String TAG_ITEM = "item";
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("start")
    @Expose
    private String start;
    @SerializedName("finish")
    @Expose
    private String finish;
    @SerializedName("spot_id")
    @Expose
    private Integer spotId;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("sponsor")
    @Expose
    private String sponsor;
    @SerializedName("pivot")
    @Expose
    private Pivot pivot;
    @SerializedName("spot")
    @Expose
    private Spot spot;
    @SerializedName("speakers")
    @Expose
    private List<Speaker> speakers = null;


    public Reminder() {
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getFinish() {
        return finish;
    }

    public void setFinish(String finish) {
        this.finish = finish;
    }

    public Integer getSpotId() {
        return spotId;
    }

    public void setSpotId(Integer spotId) {
        this.spotId = spotId;
    }

    public String getType() {
        return type;
    }

    public Reminder setType(String type) {
        this.type = type;
        return this;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getSponsor() {
        return sponsor;
    }

    public void setSponsor(String sponsor) {
        this.sponsor = sponsor;
    }

    public Pivot getPivot() {
        return pivot;
    }

    public void setPivot(Pivot pivot) {
        this.pivot = pivot;
    }

    public Spot getSpot() {
        return spot;
    }

    public void setSpot(Spot spot) {
        this.spot = spot;
    }

    public List<Speaker> getSpeakers() {
        return speakers;
    }

    public void setSpeakers(List<Speaker> speakers) {
        this.speakers = speakers;
    }

    public String getScheduleDayString() {
        String dateString = "";
        String date = start;
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dt1 = null;
        try {
            dt1 = format1.parse(date);
            DateFormat format2 = new SimpleDateFormat("dd", new Locale("es", "ES"));
            dateString = format2.format(dt1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateString;
    }

    public String getScheduleWeekDayString() {
        String dateString = "";
        String date = start;
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dt1 = null;
        try {
            dt1 = format1.parse(date);
            DateFormat format2 = new SimpleDateFormat("EEEE", new Locale("es", "ES"));
            dateString = format2.format(dt1);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return dateString;
    }

    public String getStartHourString() {
        String dateString = "";
        String date = start;
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dt1 = null;
        try {
            dt1 = format1.parse(date);
            DateFormat format2 = new SimpleDateFormat("HH:mm", new Locale("es", "ES"));
            dateString = format2.format(dt1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateString;
    }

    public String getStartDateString() {
        String dateString = "";
        String date = start;
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dt1 = null;
        try {
            dt1 = format1.parse(date);
            DateFormat format2 = new SimpleDateFormat("EE dd MM HH:mm", new Locale("es", "ES"));
            dateString = format2.format(dt1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateString;
    }

    public String getFinishHourString() {
        String dateString = "";
        String date = finish;
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dt1 = null;
        try {
            dt1 = format1.parse(date);
            DateFormat format2 = new SimpleDateFormat("HH:mm", new Locale("es", "ES"));
            dateString = format2.format(dt1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateString;
    }

    public String getDate() {
        String dateString = "";
        String date = start;
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dt1 = null;
        try {
            dt1 = format1.parse(date);
            DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd", new Locale("es", "ES"));
            dateString = format2.format(dt1);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return dateString;
    }
}
