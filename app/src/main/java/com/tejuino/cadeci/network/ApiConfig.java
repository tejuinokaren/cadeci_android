package com.tejuino.cadeci.network;

import com.tejuino.cadeci.BuildConfig;

import java.util.HashMap;

import static com.tejuino.cadeci.utils.SessionManager.sAuthToken;

/**
 * Created by dianakarenms on 5/27/17.
 */

public class ApiConfig {

    public static final String baseUrl = BuildConfig.API_URL;
    public static final String clientId = BuildConfig.CLIENT_ID;
    public static final String clientSecret = BuildConfig.CLIENT_SECRET;

    // News posts
    public static final String auth = baseUrl +  "oauth/token";
    public static final String registerUser = baseUrl + "api/v1/users";
    public static final String logout = baseUrl + "api/v1/users/logout?device_id=";
    public static final String userProfile = baseUrl + "api/v1/users/profile?os=android&fcm_token=";
    public static final String posts = baseUrl + "api/v1/posts";
    public static final String lastComments = baseUrl + "api/v1/posts/%d/lasts_comments";
    public static final String schedule = baseUrl + "api/v1/schedules";
    public static final String saveReminder = baseUrl + "api/v1/schedules/%d/save_reminder";
    public static final String deleteReminder = baseUrl + "api/v1/schedules/%d/delete_reminder";
    public static final String myReminders = baseUrl + "api/v1/schedules/my_reminders";
    public static final String sponsors = baseUrl + "api/v1/sponsors";
    public static final String comments = baseUrl + "api/v1/posts/%d/comments";
    public static final String orderSpecializations = baseUrl + "api/v1/orders/professions";
    public static final String addInscription = baseUrl + "api/v1/orders/add";
    public static final String removeInscription = baseUrl + "api/v1/orders/remove?id=%d";
//    public static final String currentOrder = baseUrl + "api/v1/orders/order";
    public static final String ordersList = baseUrl + "api/v1/orders/list";
    public static final String invoice = baseUrl + "api/v1/orders/invoice";
    public static final String payment = baseUrl + "api/v1/orders/pay";
    public static final String countries = baseUrl + "api/v1/locations/countries";
    public static final String statesByCountryId = baseUrl + "api/v1/locations/countries/%d";
    public static final String citiesBystateId = baseUrl + "api/v1/locations/state/%d";
    public static final String recoverPassword = baseUrl + "api/v1/users/recover-request";
    public static final String updatePassword = baseUrl + "api/v1/users/update-password";
    public static final String lastCongress = baseUrl + "api/v1/congress/last";
    public static final String updateFcmToken = baseUrl + "api/v1/congress/last";

    /**
     * Unauthenticated header for all api requests
     * @return
     */
    public static HashMap<String, String> getUnauthenticatedHeaders() {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/x-www-form-urlencoded");
        return  headers;
    }

    /**
     * Authenticated header for all api requests, makes use of users' access token
     * @return
     */
    public static HashMap<String, String> getAuthHeaders() {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Authorization", "Bearer " + sAuthToken.getInstance().getAccessToken());
        headers.put("Content-Type", "application/x-www-form-urlencoded");
        return  headers;
    }
}