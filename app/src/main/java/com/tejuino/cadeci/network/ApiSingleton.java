package com.tejuino.cadeci.network;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageLoader;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.tejuino.cadeci.models.ApiResponse;
import com.tejuino.cadeci.models.AuthToken;
import com.tejuino.cadeci.R;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import static com.tejuino.cadeci.utils.Repository.hideProgressFlower;
import static com.tejuino.cadeci.utils.Repository.showProgressFlower;

/**
 * Created by clickaboom on 5/28/17.
 */

public class ApiSingleton {
    private static ApiSingleton mInstance;
    private static Dialog dialogTransparent;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private static Context mCtx;

    private ApiSingleton() {
        mRequestQueue = getRequestQueue();
        mImageLoader = new ImageLoader(mRequestQueue, new BitmapLruCache());
    }

    public static synchronized ApiSingleton getInstance(Context context) {
        mCtx = context;
        if (mInstance == null) {
            mInstance = new ApiSingleton();
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            Cache cache = new DiskBasedCache(mCtx.getCacheDir(), 10 * 1024 * 1024);
            Network network = new BasicNetwork(new HurlStack());
            mRequestQueue = new RequestQueue(cache, network);
            // Don't forget to start the volley request queue
            mRequestQueue.start();
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    public static class GsonRequest<T> extends Request<T> {
        private final Gson gson = new Gson();
        private final Class<T> clazz;
        private final Map<String, String> headers;
        private final Map<String, String> params;
        private final Response.Listener<T> listener;
        private final boolean showProgress;

        /**
         * Make a GET request and return a parsed object from JSON.
         *
         * @param url     URL of the request to make
         * @param clazz   Relevant class object, for Gson's reflection
         * @param method  Method.GET or Method.POST
         * @param headers Map of request headers
         * @param params  Map of request parameters
         */
        public GsonRequest(String url, Class<T> clazz, int method, Map<String, String> headers, Map<String, String> params, boolean showProgress,
                           Response.Listener<T> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);

            // Set Volley retry policy
            setRetryPolicy(new DefaultRetryPolicy(
                    0, // this prevents sending data twice // DefaultRetryPolicy.DEFAULT_TIMEOUT_MS
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


            this.clazz = clazz;
            this.headers = headers;
            this.params = params;
            this.listener = listener;
            this.showProgress = showProgress;
            if(showProgress)
                showProgressFlower(mCtx);
        }

        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            return headers != null ? headers : super.getHeaders();
        }

        @Override
        public Map<String, String> getParams() throws AuthFailureError {
            return params != null ? params : super.getParams();
        }

        @Override
        protected void deliverResponse(T response) {
            listener.onResponse(response);
        }

        @Override
        protected Response<T> parseNetworkResponse(NetworkResponse response) {
            if(showProgress)
                hideProgressFlower();

            try {
                String json = new String(
                        response.data,
                        HttpHeaderParser.parseCharset(response.headers));
                return Response.success(
                        gson.fromJson(json, clazz),
                        HttpHeaderParser.parseCacheHeaders(response));
            } catch (UnsupportedEncodingException e) {
                return Response.error(new ParseError(e));
            } catch (JsonSyntaxException e) {
                return Response.error(new ParseError(e));
            }
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            if(showProgress)
                hideProgressFlower();

            String externalError = "";
            try {
                if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {
                    Log.e("ApiSingleton", "TimeoutError|NoConnectionError");
                    externalError = mCtx.getString(R.string.error_network_timeout);

                } else if (volleyError instanceof AuthFailureError) {
                    Log.e("ApiSingleton", "AuthFailureError");
                    NetworkResponse response = volleyError.networkResponse;
                    if (response.data != null) {
                        String json = new String(response.data);
                        final AuthToken res = gson.fromJson(json, AuthToken.class);
                        externalError = res.message;
                    }

                } else if (volleyError instanceof ServerError) {
                    Log.e("ApiSingleton", "ServerError");
                    NetworkResponse response = volleyError.networkResponse;
                    if (response.data != null) {
                        String json = new String(response.data);
                        final ApiResponse res = gson.fromJson(json, ApiResponse.class);
                        if(res != null) {
                            externalError = res.error.external;
                        } else {
                            externalError = "Error del servidor.";
                        }
                    }

                } else if (volleyError instanceof NetworkError) {
                    Log.e("ApiSingleton", "NetworkError");

                } else if (volleyError instanceof ParseError) {
                    Log.e("ApiSingleton", "ParseError");

                }

                // Show error message to user
                final String finalExternalError = externalError;
                ((Activity) mCtx).runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(mCtx,
                                finalExternalError,
                                Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (JsonSyntaxException e) {
                Log.e("Api - server error", e.getMessage());
            }

            return super.parseNetworkError(volleyError);
        }
    }

    /**
     * Get loader for images with volley
     *
     * @return ImageLoader
     */
    public ImageLoader getImageLoader() {
        return mImageLoader;
    }

    /**
     * Bitmap setup for Volley image loading
     */
    public static class BitmapLruCache extends LruCache<String, Bitmap> implements ImageLoader.ImageCache {
        public static int getDefaultLruCacheSize() {
            final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
            final int cacheSize = maxMemory / 8;

            return cacheSize;
        }

        public BitmapLruCache() {
            this(getDefaultLruCacheSize());
        }

        public BitmapLruCache(int sizeInKiloBytes) {
            super(sizeInKiloBytes);
        }

        @Override
        protected int sizeOf(String key, Bitmap value) {
            return value.getRowBytes() * value.getHeight() / 1024;
        }

        @Override
        public Bitmap getBitmap(String url) {
            return get(url);
        }

        @Override
        public void putBitmap(String url, Bitmap bitmap) {
            put(url, bitmap);
        }
    }
}