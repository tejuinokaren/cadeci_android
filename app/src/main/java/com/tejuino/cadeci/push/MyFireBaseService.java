package com.tejuino.cadeci.push;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.tejuino.cadeci.models.ApiResponse;
import com.tejuino.cadeci.network.ApiConfig;
import com.tejuino.cadeci.network.ApiSingleton;
import com.tejuino.cadeci.utils.SessionManager;

import java.util.HashMap;
import java.util.LinkedHashMap;

import static com.tejuino.cadeci.utils.SessionManager.sSession;


/**
 * Created by Fabian on 10/30/17.
 */

public class MyFireBaseService extends FirebaseInstanceIdService {


    private String TAG = "MyFirebaseService";
    private String token = FirebaseInstanceId.getInstance().getToken();

    @Override
    public void onTokenRefresh() {
        if(!SessionManager.getInstance(this).isLoggedIn()) return;

        HashMap<String, String> params = new LinkedHashMap<>();
        params.put("os", "android");
        params.put("token", token);
        params.put("device_id", sSession.getDeviceId());

        ApiSingleton.getInstance(this).addToRequestQueue(
                new ApiSingleton.GsonRequest(ApiConfig.updateFcmToken,
                ApiResponse.class,
                Request.Method.POST,
                ApiConfig.getAuthHeaders(),
                        params,
                        true,
                    new Response.Listener() {
                        @Override
                        public void onResponse(Object response) {
                            ApiResponse res = (ApiResponse) response;
                            sSession.setDeviceId(res.data.deviceId);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d(TAG, error.toString());
                        }
                }));
    }
}
