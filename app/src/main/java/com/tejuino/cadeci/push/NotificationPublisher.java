package com.tejuino.cadeci.push;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.google.firebase.messaging.RemoteMessage;
import com.tejuino.cadeci.R;


/**
 * Created by moyhdez on 29/12/16.
 */

public class NotificationPublisher {

    private static NotificationPublisher shared;

    public static NotificationPublisher getInstance() {
        if (shared == null) {
            shared = new NotificationPublisher();
        }
        return shared;
    }

    private NotificationPublisher() {

    }

    public void showNotification(Context context, RemoteMessage remoteMessage) {
        Intent intent = new Intent();
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, buildNotification(context, pendingIntent,
                remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody()).build());

    }

    private NotificationCompat.Builder buildNotification(Context context, PendingIntent pendingIntent, String title, String content){
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setColor(ContextCompat.getColor(context,R.color.colorPrimary))
                .setContentTitle(title)
                .setContentText(content)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setContentIntent(pendingIntent);

        return notificationBuilder;
    }

}
