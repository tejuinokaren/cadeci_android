package com.tejuino.cadeci.utils;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.tejuino.cadeci.BuildConfig;

import java.math.BigDecimal;

/**
 * Created by dianakarenms on 11/05/17.
 */

public class PaypalHelper {
    private static final String currency = "MXN";
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;

    private static String getEnviroment(){
        if(BuildConfig.PAYPAL_ENVIROMENT.equals("production")){
            return PayPalConfiguration.ENVIRONMENT_PRODUCTION;
        }else if(BuildConfig.PAYPAL_ENVIROMENT.equals("sandbox")){
            return PayPalConfiguration.ENVIRONMENT_SANDBOX;
        }else{
            return PayPalConfiguration.ENVIRONMENT_NO_NETWORK;
        }
    }

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(getEnviroment())
            .clientId(BuildConfig.PAYPAL_CLIENT_ID)
            // The following are only used in PayPalFuturePaymentActivity.
            .merchantName("CADECI")
            .merchantPrivacyPolicyUri(Uri.parse("http://cadeci.org.mx/files/aviso-de-privacidad-cadeci.pdf"))
            .merchantUserAgreementUri(Uri.parse("http://cadeci.org.mx/files/aviso-de-privacidad-cadeci.pdf"));

    public static Intent createPaymentIntent(Activity activity, String concept, double total, String currency){
        PayPalPayment thingToBuy = new PayPalPayment(new BigDecimal(total), currency, concept,
                PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(activity, PaymentActivity.class);
        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);

        return intent;
    }

    public static Intent createFuturePaymentIntent(Activity activity, String concept, double total){
        PayPalPayment thingToBuy = new PayPalPayment(new BigDecimal(total), currency, concept,
                PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(activity, PayPalFuturePaymentActivity.class);
        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);

        return intent;
    }

}
