package com.tejuino.cadeci.utils;

/**
 * Created by dianakarenms on 11/04/17.
 */

/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.tejuino.cadeci.activities.LoginActivity;
import com.tejuino.cadeci.activities.MainActivity;
import com.tejuino.cadeci.models.ApiResponse;
import com.tejuino.cadeci.models.AuthToken;
import com.tejuino.cadeci.models.User;
import com.tejuino.cadeci.network.ApiConfig;
import com.tejuino.cadeci.network.ApiSingleton;
import com.tejuino.cadeci.R;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static android.content.ContentValues.TAG;


public class SessionManager {
    // Instance for singleton
    private static SessionManager sInstance = null;

    // Shared Preferences
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "mx.tejuino.cadeci";

    // All Shared Preferences Keys (make variable public to access from outside)
    private static final String IS_LOGIN = "IsLoggedIn";

    // User values
    public static final String KEY_USER = "user";
    public static final String KEY_DEVICE_ID= "device_id";
    public static final String KEY_AUTH_TOKEN = "auth_token";
    public static long ONE_MINUTE_IN_MILLIS = 60000;//millisecs

    private static Context _context;
    public static SessionManager sSession;
    public static User sUser;
    public static int deviceId;
    public static AuthToken sAuthToken;
    private static boolean sSplashFlag;

    public static SessionManager getInstance(Context context) {
        _context = context;
        if (sInstance == null) {
            synchronized (SessionManager.class) {
                if (sInstance == null) {
                    sInstance = new SessionManager();
                    sSession = sInstance;
                    sAuthToken = sSession.loadTokenFromPrefs();
                    sUser = sSession.loadUserFromPrefs();
                }
            }
        }
        return sInstance;
    }

    // Constructor
    private SessionManager(){
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public static void setSplashFlag(boolean splashFlag) {
        sSplashFlag = splashFlag;
    }

    /**
     * Create login sSession
     * */
    public void createLoginSession(){
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing sUser in pref
        Gson gson = new Gson();
        String json = gson.toJson(sUser.getInstance());
        editor.putString(KEY_USER, json);

        editor.putInt(KEY_DEVICE_ID, deviceId);

        json = gson.toJson(sAuthToken.getInstance());
        editor.putString(KEY_AUTH_TOKEN, json);

        // commit changes
        editor.commit();
    }

    /**
     *
     * @param authToken
     */
    public void updateAuthtoken(AuthToken authToken){
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing sUser in pref
        Gson gson = new Gson();
        String json = gson.toJson(authToken);
        editor.putString(KEY_AUTH_TOKEN, json);

        // commit changes
        editor.commit();
    }

    /**
     * Check login method wil check sUser login status
     * If false it will redirect sUser to login page
     * Else won't do anything
     * */
    public void checkLogin(){
        Intent i;
        // Check login status
        if(!this.isLoggedIn()){
            // sUser is not logged in, redirect him to Login Activity
            i = LoginActivity.newIntent(_context);
        } else {
            try {
                if (Calendar.getInstance().getTime().compareTo(sAuthToken.getInstance().getExpiresAtDate()) >= 0) { // Token has expired, or expires immediately
                    sSession.setTokenRefreshTimer((Activity) _context, 0);
                    return;
                } else {
                    sSession.setTokenRefreshTimer((Activity) _context, getRemainTime());
                }
            } catch (NullPointerException ex) {
                return;
            }

            // sUser is logged in, redirect him to Main Activity
            // and clean all previous activities
            i = MainActivity.newIntent(_context);
        }

        // Closing all the Activities
        if(sSplashFlag) {
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            sSplashFlag = false;
        } else {
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }

        // Staring Login Activity
        _context.startActivity(i);
    }

    /**
     * Clear sSession details
     * */
    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect sUser to Login Activity
        Intent i = new Intent(_context, LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        logoutUserApi(_context);
        // Starting Login Activity
        _context.startActivity(i);
    }

    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        Boolean isLoggedIn = pref.getBoolean(IS_LOGIN, false);
        return isLoggedIn;
    }

    public User loadUserFromPrefs() {
        if(isLoggedIn()) {
            Gson gson = new Gson();
            String json = pref.getString(KEY_USER, "");
            User.setInstance(gson.fromJson(json, User.class));

            json = pref.getString(KEY_AUTH_TOKEN, "");
            AuthToken.setInstance(gson.fromJson(json, AuthToken.class));
        }
        return User.getInstance();
    }

    public AuthToken loadTokenFromPrefs() {
        if(isLoggedIn()) {
            Gson gson = new Gson();
            String json = pref.getString(KEY_AUTH_TOKEN, "");
            AuthToken.setInstance(gson.fromJson(json, AuthToken.class));
        }
        return AuthToken.getInstance();
    }

    public String getDeviceId(){
        if(isLoggedIn()){
            int id = pref.getInt(KEY_DEVICE_ID, -1);
            if(id == -1) return "";
            else return String.valueOf(id);
        }
        return "";
    }

    public void setDeviceId(int id){
        editor.putInt(KEY_DEVICE_ID, id);
    }

    /**
     * Session variables management
     */
    public void auth(Context context){
        _context = context;

//        ApiSingleton.showProgressFlower((Activity) _context);
        Map<String, String> params = new HashMap<>();
        params.put("grant_type", "password");
        params.put("client_id", ApiConfig.clientId);
        params.put("client_secret", ApiConfig.clientSecret);
        params.put("username", sUser.getInstance().getEmail());
        params.put("password", sUser.getInstance().getPassword());
        params.put("scope", "");

        // Access the RequestQueue through your singleton class.
        ApiSingleton.getInstance(_context)
                .addToRequestQueue(new ApiSingleton.GsonRequest(ApiConfig.auth,
                        AuthToken.class,
                        Request.Method.POST,
                        ApiConfig.getUnauthenticatedHeaders(), params, true,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());
//                                ApiSingleton.hideProgressFlower();

                                AuthToken res = (AuthToken) response;
                                if(res.getAccessToken()!= null) {
                                    int expires = res.getExpiresIn() - 10;
                                    res.setExpiresAtDate(sumMinutesToDate(expires));
                                    sAuthToken.setInstance(res);
                                    setTokenRefreshTimer((Activity) _context, getRemainTime());

                                    getUserProfile(true);
                                } else {
                                    Toast.makeText(_context, _context.getResources().getString(R.string.auth_error), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
//                        ApiSingleton.hideProgressFlower();
                    }
                }));
    }

    public void authRefreshToken(final Activity activity) {
        Map<String, String> params = new HashMap<>();
        params.put("refresh_token", sAuthToken.getInstance().getRefreshToken());
        // Access the RequestQueue through your singleton class.
        ApiSingleton.getInstance(_context)
                .addToRequestQueue(new ApiSingleton.GsonRequest(ApiConfig.auth,
                        AuthToken.class,
                        Request.Method.POST,
                        ApiConfig.getAuthHeaders(), params, true,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());

                                Gson gson = new Gson();
                                AuthToken res = gson.fromJson(response.toString(), AuthToken.class);
                                int expires = res.getExpiresIn() - 10;
                                //int expires = 5;
                                res.setExpiresAtDate(sumMinutesToDate(expires));
                                sAuthToken.setInstance(res);
                                setTokenRefreshTimer(activity, getRemainTime());

                                sSession.updateAuthtoken(sAuthToken);
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                    }
                }));
    }

    public static void logoutUserApi(Context activity){

        ApiSingleton.getInstance(activity)
                                .addToRequestQueue(new ApiSingleton.GsonRequest(
                                        ApiConfig.logout.concat(String.valueOf(deviceId)),
                        ApiResponse.class,
                        Request.Method.POST,
                        ApiConfig.getAuthHeaders(), null, false,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d(TAG, error.toString());

                            }
                }));
    }

    private Date sumMinutesToDate(int minutes) {
        // Set expiration date
        Calendar date = Calendar.getInstance();
        long t = date.getTimeInMillis();
        Date expiresAt = new Date(t + minutes*ONE_MINUTE_IN_MILLIS ); // Add expiration minutes to current date - 10 minutes
        return expiresAt;
    }

    public void setTokenRefreshTimer(final Activity activity, int remaingMinutes) {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                // this code will be executed after 2 seconds
                authRefreshToken(activity);
            }
        }, remaingMinutes*ONE_MINUTE_IN_MILLIS);
    }

    public static int getRemainTime() {
        long curDate = Calendar.getInstance().getTimeInMillis();
        int remainTime = (int)((sAuthToken.getInstance().getExpiresAtDate().getTime()/ONE_MINUTE_IN_MILLIS) - (curDate/ONE_MINUTE_IN_MILLIS));
        return remainTime;
    }

    public void getUserProfile(final boolean isLoginAction) {

        String fcmToken = FirebaseInstanceId.getInstance().getToken();
        if(fcmToken == null) fcmToken = "";

        // Access the RequestQueue through your singleton class.
        ApiSingleton.getInstance(_context)
                .addToRequestQueue(new ApiSingleton.GsonRequest(
                        ApiConfig.userProfile.concat(fcmToken),
                        ApiResponse.class,
                        Request.Method.GET,
                        ApiConfig.getAuthHeaders(), null, true,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                Log.d(TAG, response.toString());
                                ApiResponse res = (ApiResponse) response;
                                sUser.setInstance(res.data.user);
                                deviceId  = res.data.deviceId;

                                createLoginSession();

                                // Get token
//                                String token = FirebaseInstanceId.getInstance().getToken();
//                                String deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
//                                MyFirebaseInstanceIDService.sendRegistrationToServer(token, deviceId, context);

                                if(isLoginAction)
                                    checkLogin();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                    }
                }));
    }
}