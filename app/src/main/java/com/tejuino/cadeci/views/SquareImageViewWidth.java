package com.tejuino.cadeci.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

/**
 * Created by karen on 30/06/17.
 */

public class SquareImageViewWidth extends android.support.v7.widget.AppCompatImageView {
    public SquareImageViewWidth(Context context) {
        super(context);
    }

    public SquareImageViewWidth(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareImageViewWidth(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth());
    }
}
